package cl.forus.volumetria.controller;

import cl.forus.volumetria.services.AppService;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Controller
public class MainController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    @Autowired
    AppService appService;

    @ResponseBody
    @GetMapping("/checkQuantityAndTagNumber")
    public List<Object> checkQuantityAndTagNumber(@RequestParam("tag_number") String tagNumber) {
        return appService.checkQuantityAndTagNumber(tagNumber);
    }

    @ResponseBody
    @GetMapping("/requestTypeHomework")
    public List<Object> requestTypeHomework(@RequestParam("tag_number") String tagNumber) {
        return appService.requestHomeworkList(tagNumber);
    }

    @ResponseBody
    @GetMapping("/checkSKU")
    public List<Object> checkSKU(@RequestParam("sku") String sku) {
        return appService.isSKUValid(sku);
    }

    @ResponseBody
    @GetMapping("/requestSKUValidationFromMasterBox")
    public Boolean requestSKUValidationFromMasterBox(@RequestParam("sku") String sku) {
        return appService.requestSKUValidationFromMasterBox(sku);
    }

    @ResponseBody
    @GetMapping("/requestSKUModelCode")
    public String requestSKUModelCode(@RequestParam("sku") String sku) {
        return appService.requestSKUModelCode(sku);
    }

/*    @ResponseBody
    @GetMapping("/checkSKU")
    public int checkSKU(@RequestParam("sku") String sku) {
        BigDecimal operation = appService.isSKUValid(sku);
        int result = 0;
        if (operation.compareTo(BigDecimal.ZERO) != 0) {
            System.out.println("mayor a cero");
            result = 1;
        }
        return result;
    }*/

    @ResponseBody
    @GetMapping("/requestPreviousRecord")
    public List<Object> requestPreviousRecord(@RequestParam("tagNumber") String tagNumber) {
        return appService.requestPreviousRecord(tagNumber);
    }

    @ResponseBody
    @GetMapping("/saveNewRecord")
    public Boolean saveNewRecord(@RequestParam("volume") String[] volume, @RequestParam("isOnlySku") Boolean isOnlySku) {
        return appService.saveNewRecord(volume, isOnlySku);
    }

    @ResponseBody
    @GetMapping("/requestPreviousRecordBySKUAndTagNumber")
    public List<Object> requestPreviousRecordBySKUAndTagNumber(
            @RequestParam("sku") String sku
    ) {
        return appService.requestPreviousRecordBySKUAndTagNumber(sku);
    }

    @ResponseBody
    @GetMapping("/saveNewRecordLongSku")
    public Boolean saveNewRecordLongSku(
            @RequestParam("volume") String[] volume
    ) {
        return appService.saveNewRecordLongSku(volume);
    }

    @ResponseBody
    @GetMapping("/requestPreviousRecordBySKU")
    public List<Object> requestPreviousRecordBySKU(@RequestParam("sku") String sku) {
        return appService.requestPreviousRecordBySKU(sku);
    }

    @ResponseBody
    @GetMapping("/updateSkuWeightIntoPmf")
    public Boolean updateSkuWeightIntoPmf(@RequestParam("sku") BigDecimal sku, @RequestParam("weight") Float weight) {
        return appService.updateSkuWeightIntoPmf(sku, weight);
    }

    @ResponseBody
    @GetMapping("/updateHomeworkWeightIntoPmf")
    public Boolean updateHomeworkWeightIntoPmf(@RequestParam("sku") BigDecimal sku, @RequestParam("weight") Float weight) {
        return appService.updateHomeworkWeightIntoPmf(sku, weight);
    }

    @ResponseBody
    @GetMapping("/checkWeight")
    public String[] checkWeight(@RequestParam("sku") String sku) {
        //[0.3, 0, 0, 0, -0.015, 0, 044211499758, 6, 1, , , , , 0.3, 0, 0, -0.015 ]
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.add("Content-Type", "application/json");
        httpHeaders.add("Access-Control-Allow-Origin", "*");
        httpHeaders.add("Access-Control-Allow-Credentials", "true");
        httpHeaders.add("Access-Control-Allow-Methods", "*");
        httpHeaders.add("Access-Control-Allow-Headers", "*");
        httpHeaders.add("Content-Type", "application/javascript");

        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);

        RestTemplate restTemplate = new RestTemplate();

        String uri = "http://172.16.16.187:8080";
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, String.class);

        String[] responseBody = Objects.requireNonNull(response.getBody().replace(",", ".").replace("\r\n", "")).split(";");

        System.out.println("request parcel cube " + Arrays.toString(responseBody));

        //BigDecimal result = appService.isSKUValid();

        //appService.insertProductVolume(responseBody, sku);
        //System.out.println("result es: " + result);
        return responseBody;
    }

    @ResponseBody
    @GetMapping("/insertProductVolume")
    public Boolean insertProductVolume(
            @RequestParam("volume") String[] volume,
            @RequestParam("status") Boolean status
    ) {
        return appService.insertProductVolume(volume, status);
    }

    @ResponseBody
    @GetMapping("/insertProductVolumeByUnityFromMasterBox")
    public Boolean insertProductVolumeByUnityFromMasterBox(
            @RequestParam("volume") String[] volume,
            @RequestParam("status") Boolean status
    ) {
        return appService.insertProductVolumeByUnityFromMasterBox(volume, status);
    }


    @ResponseBody
    @GetMapping("/checkRecordBySKU")
    public BigDecimal checkRecordBySKU(@RequestParam(name = "sku") String sku) {
        System.out.println("sku data type " + BigDecimal.valueOf(Long.parseLong(sku)));
        return appService.checkRecordBySKU(BigDecimal.valueOf(Long.parseLong(sku)));
    }

    @ResponseBody
    @GetMapping("/checkRecordByUnitySKU")
    public BigDecimal checkRecordByUnitySKU(@RequestParam("sku") String sku) {
        return appService.checkRecordByUnitySKU(sku);
    }

    @ResponseBody
    @GetMapping("/insertProductVolumeByTagNumber")
    public Boolean insertProductVolumeByTagNumber(
            @RequestParam("volume") String[] volume,
            @RequestParam("status") Boolean status
    ) {
        System.out.println("valores volumen backend: " + volume);
        System.out.println("status backend: " + status);
        return appService.insertProductVolumeByTagNumber(volume, status);
    }

    @ResponseBody
    @GetMapping("/saveWeightIntoPmf")
    public Boolean saveWeightIntoPmf(@RequestParam("weight") String weight, @RequestParam("sku") String sku) {
        System.out.println("el peso es: " + weight);
        return appService.saveWeightIntoPmf(weight, sku);
    }

    @ResponseBody
    @GetMapping("/requestAuthentication")
    public List<Object> requestAuthentication(@RequestParam("user") String user, @RequestParam("password") String password) {
        return appService.requestAuthentication(user, password);
    }

    @ResponseBody
    @GetMapping("/requestDuplicatedProducts")
    public List<Object> requestDuplicatedProducts() {
        return appService.requestDuplicatedProducts();
    }

    @ResponseBody
    @GetMapping("/requestOriginalRecord")
    public List<Object> requestOriginalRecord(@RequestParam("sku") BigDecimal sku) {
        return appService.requestOriginalRecord(sku);
    }

    @ResponseBody
    @GetMapping("/requestDeclineNewVolume")
    public Boolean requestDeclineNewVolume(@RequestParam("sku") BigDecimal sku) {
        return appService.requestDeclineNewVolume(sku);
    }

    @ResponseBody
    @GetMapping("/requestSaveNewVolume")
    public Boolean requestSaveNewVolume(@RequestParam("sku") String[] newVolume) {
        return appService.requestSaveNewVolume(newVolume);
    }

    @GetMapping("/")
    public ModelAndView main() {
        return new ModelAndView("main");
    }

    @GetMapping("/admin")
    public ModelAndView admin() {
        ModelAndView mav = new ModelAndView("admin");
        mav.addObject("data", appService.requestDuplicatedProducts());
        return mav;
    }
}
