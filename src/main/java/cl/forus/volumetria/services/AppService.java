package cl.forus.volumetria.services;

import cl.forus.volumetria.dao.IAppDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;

@Service
public class AppService {

    @Autowired
    IAppDao appDao;

    public List<Object> isSKUValid(String sku) {
        return appDao.isSKUValid(sku);
    }

    public List<Object> checkQuantityAndTagNumber(String tagNumber) {
        return appDao.checkQuantityAndTagNumber(tagNumber);
    }

    public Boolean insertProductVolume(String[] volume, Boolean status) {
        return appDao.insertProductVolume(volume, status);
    }

    public BigDecimal checkRecordBySKU(BigDecimal sku) {
        return appDao.checkRecordBySKU(sku);
    }

    public BigDecimal checkRecordByUnitySKU(String sku) {
        return appDao.checkRecordByUnitySKU(sku);
    }

    public Boolean insertProductVolumeByTagNumber(String[] volume, Boolean status) {
        return appDao.insertProductVolumeByTagNumber(volume, status);
    }

    public BigDecimal compareWithLastProduct(String[] responseBody) {
        return appDao.compareWithLastProduct(responseBody);
    }

    public Boolean saveWeightIntoPmf(String weight, String sku) {
        return appDao.saveWeightIntoPmf(weight, sku);
    }

    public List<Object> requestHomeworkList(String tagNumber) {
        return appDao.requestHomeworkList(tagNumber);
    }

    public List<Object> requestAuthentication(String user, String password) {
        return appDao.requestAuthentication(user, password);
    }

    public List<Object> requestDuplicatedProducts() {
        return appDao.requestDuplicatedProducts();
    }

    public List<Object> requestOriginalRecord(BigDecimal sku) {
        return appDao.requestOriginalRecord(sku);
    }

    public Boolean requestDeclineNewVolume(BigDecimal sku) {
        return appDao.requestDeclineNewVolume(sku);
    }

    public Boolean requestSaveNewVolume(String[] newVolume) {
        return appDao.requestSaveNewVolume(newVolume);
    }

    public Boolean updateSkuWeightIntoPmf(BigDecimal sku, Float weight) {
        return appDao.updateSkuWeightIntoPmf(sku, weight);
    }

    public Boolean updateHomeworkWeightIntoPmf(BigDecimal sku, Float weight) {
        return appDao.updateHomeworkWeightIntoPmf(sku, weight);
    }

    public Boolean insertProductVolumeByUnityFromMasterBox(String[] volume, Boolean status) {
        return appDao.insertProductVolumeByUnityFromMasterBox(volume, status);
    }

    public Boolean requestSKUValidationFromMasterBox(String sku) {
        return appDao.requestSKUValidationFromMasterBox(sku);
    }

    public String requestSKUModelCode(String sku) {
        return appDao.requestSKUModelCode(sku);
    }

    public List<Object> requestPreviousRecord(String tagNumber) {
        return appDao.requestPreviousRecord(tagNumber);
    }

    public Boolean saveNewRecord(String[] volume, Boolean isOnlySku) {
        return appDao.saveNewRecord(volume, isOnlySku);
    }

    public Boolean saveNewRecordLongSku(String[] volume) {
        return appDao.saveNewRecordLongSku(volume);
    }

    public List<Object> requestPreviousRecordBySKU(String sku) {
        return appDao.requestPreviousRecordBySKU(sku);
    }
    public List<Object> requestPreviousRecordBySKUAndTagNumber(String sku) {
        return appDao.requestPreviousRecordBySKUAndTagNumber(sku);
    }
}
