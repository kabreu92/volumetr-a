package cl.forus.volumetria.dao;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;

public interface IAppDao {
    public List<Object> isSKUValid(String sku);
    public Boolean insertProductVolume(String[] volume, Boolean status);
    public Boolean insertProductVolumeByTagNumber(String[] volume, Boolean status);
    public BigDecimal compareWithLastProduct(String[] responseBody);
    public List<Object> checkQuantityAndTagNumber(String tagNumber);
    public List<Object> requestHomeworkList(String tagNumber);
    public Boolean saveWeightIntoPmf(String weight, String sku);
    public BigDecimal checkRecordBySKU(BigDecimal sku);
    public BigDecimal checkRecordByUnitySKU(String sku);
    public List<Object> requestAuthentication(String user, String password);
    public List<Object> requestDuplicatedProducts();
    public List<Object> requestOriginalRecord(BigDecimal sku);
    public Boolean requestDeclineNewVolume(BigDecimal sku);
    public Boolean requestSaveNewVolume(String[] newVolume);
    public Boolean updateSkuWeightIntoPmf(BigDecimal sku, Float weight);
    public Boolean updateHomeworkWeightIntoPmf(BigDecimal sku, Float weight);
    public Boolean insertProductVolumeByUnityFromMasterBox(String[] volume, Boolean status);
    public Boolean requestSKUValidationFromMasterBox(String sku);
    public String requestSKUModelCode(String sku);
    public List<Object> requestPreviousRecord(String tagNumber);
    public Boolean saveNewRecord(String[] volume, Boolean isOnlySku);
    public Boolean saveNewRecordLongSku(String[] volume);
    public List<Object> requestPreviousRecordBySKU(String sku);
    public List<Object> requestPreviousRecordBySKUAndTagNumber(String sku);
}