package cl.forus.volumetria.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Repository
@Transactional
public class AppImpl implements IAppDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppImpl.class);

    @PersistenceContext(unitName = "oracleDB")
    private EntityManager entityManager;

 /*   @Override
    public BigDecimal isSKUValid(String sku) {
        //780622026598
        Query query = entityManager.createNativeQuery("SELECT COUNT(1) FROM producto_sku WHERE id_sku = " + sku);
        return (BigDecimal) query.getSingleResult();
    }*/

    @Override
    public List<Object> isSKUValid(String sku) {
        //780622026598
        sku = sku.replaceFirst("^0+(?!$)", "");
        sku = sku.substring(0, sku.length() - 1);
        System.out.println("el sku validado: " + sku);
        Query query = entityManager.createNativeQuery("SELECT DISTINCT (CASE WHEN tipo_sku = 'T' THEN cantidad END) qty,\n" +
                "                id_sku,\n" +
                "                tipo_sku,\n" +
                "                cod_modelo,\n" +
                "                cod_color,\n" +
                "                modelo,\n" +
                "                color,\n" +
                "                temporada,\n" +
                "                (CASE WHEN tipo_sku = 'S' THEN talla_numero END) talla,\n" +
                "                codigo_barra\n" +
                "  FROM producto_sku s, producto_s_atribut p, producto b\n" +
                " WHERE     id_sku IN (" + sku + ")\n" +
                "       AND s.id_producto = p.id_producto\n" +
                "       AND s.id_producto = b.id_producto");
        return (List<Object>) query.getResultList();
    }

    @Override
    public String requestSKUModelCode(String sku) {
        Query query = entityManager.createNativeQuery("SELECT DISTINCT a.cod_modelo\n" +
                "  FROM producto_s_atribut a, producto_sku s\n" +
                " WHERE s.id_sku = " + sku + " AND a.id_producto = s.id_producto");
        return (String) query.getSingleResult();
    }

    @Override
    public Boolean requestSKUValidationFromMasterBox(String sku) {
        Query query = entityManager.createNativeQuery("SELECT *\n" +
                "  FROM producto_volumen\n" +
                " WHERE id_sku = " + sku + " AND numero_tag IS NULL");
        return query.getResultList().size() > 0;
    }

    @Override
    public Boolean insertProductVolume(String[] volume, Boolean status) {
        int param = status ? 0 : 1;
        Query query = entityManager.createNativeQuery("INSERT INTO producto_volumen (id_sku,\n" +
                "                              largo,\n" +
                "                              ancho,\n" +
                "                              volumen,\n" +
                "                              peso,\n" +
                "                              volumen_peso,\n" +
                "                              contenedor,\n" +
                "                              altura,\n" +
                "                              status,\n" +
                "                              cod_modelo)\n" +
                "     VALUES (" + volume[0] + ",\n" +
                "             " + Float.parseFloat(volume[1]) + ",\n" +
                "             " + Float.parseFloat(volume[2]) + ",\n" +
                "             " + Float.parseFloat(volume[3]) + ",\n" +
                "             " + Float.parseFloat(volume[4]) + ",\n" +
                "             " + Float.parseFloat(volume[5]) + ",\n" +
                "             '" + volume[6] + "',\n" +
                "             " + Float.parseFloat(volume[7]) + ",\n" +
                "             " + param + ",\n" +
                "             '" + volume[8] + "')");
        return query.executeUpdate() > 0;
    }

    @Override
    public BigDecimal checkRecordBySKU(BigDecimal sku) {
        Query query = entityManager.createNativeQuery("SELECT COUNT (id_sku)\n" +
                "  FROM producto_volumen\n" +
                " WHERE status = 1 AND id_sku =" + sku);
        return (BigDecimal) query.getSingleResult();
    }

    @Override
    public List<Object> requestPreviousRecord(String tagNumber) {
        Query query = entityManager.createNativeQuery(" SELECT * FROM producto_volumen WHERE numero_tag = '" + tagNumber + "' AND status = 1");
        return query.getResultList();
    }

    @Override
    public List<Object> requestPreviousRecordBySKU(String sku) {
        Query query = entityManager.createNativeQuery(" SELECT * FROM producto_volumen WHERE numero_tag IS NULL AND status = 1 AND id_sku = " + sku);
        return query.getResultList();
    }

    @Override
    public List<Object> requestPreviousRecordBySKUAndTagNumber(String sku) {
        Query query = entityManager.createNativeQuery("SELECT * FROM producto_volumen WHERE status = 1 AND id_sku = " + sku);
        return query.getResultList();
    }

    /*    @Override
    public BigDecimal checkRecordByUnitySKU(String sku) {
        Query query = entityManager.createNativeQuery("SELECT COUNT (cod_modelo) codigo_modelo\n" +
                "  FROM producto_sku s, producto_s_atribut p\n" +
                " WHERE cod_modelo = '" + sku + "' AND s.id_producto = p.id_producto");
        return (BigDecimal) query.getSingleResult();
    }*/

    @Override
    public BigDecimal checkRecordByUnitySKU(String sku) {
        Query query = entityManager.createNativeQuery("SELECT COUNT (cod_modelo) codigo_modelo\n" +
                "  FROM producto_sku s, producto_s_atribut p\n" +
                " WHERE cod_modelo = '" + sku + "' AND s.id_producto = p.id_producto");
        return (BigDecimal) query.getSingleResult();
    }

    @Override
    public Boolean saveNewRecord(String[] volume, Boolean isOnlySku) {
        System.out.println("valores array " + Arrays.toString(volume));
        Boolean updatePreviousRecords = isOnlySku ? this.updatePreviousRecordsBySku(volume[9]) : this.updatePreviousRecords(volume[0]);
        if (updatePreviousRecords) {
            Query query = entityManager.createNativeQuery("INSERT INTO producto_volumen (id_sku, numero_tag,\n" +
                    "                              largo,\n" +
                    "                              ancho,\n" +
                    "                              volumen,\n" +
                    "                              peso,\n" +
                    "                              volumen_peso,\n" +
                    "                              contenedor,\n" +
                    "                              altura,\n" +
                    "                              unidades_caja,\n" +
                    "                              status,\n" +
                    "                              cod_modelo)\n" +
                    "     VALUES (" + volume[9] + ",\n" +
                    "             '" + volume[0] + "',\n" +
                    "             " + Float.parseFloat(volume[1]) + ",\n" +
                    "             " + Float.parseFloat(volume[2]) + ",\n" +
                    "             " + Float.parseFloat(volume[3]) + ",\n" +
                    "             " + Float.parseFloat(volume[4]) + ",\n" +
                    "             " + Float.parseFloat(volume[5]) + ",\n" +
                    "             '" + volume[6] + "',\n" +
                    "             " + Float.parseFloat(volume[7]) + ",\n" +
                    "             " + volume[8] + ",\n" +
                    "             " + 1 + ",\n" +
                    "             '" + volume[10] + "')");
            return query.executeUpdate() > 0;
        } else {
            return false;
        }
    }

    @Override
    public Boolean saveNewRecordLongSku(String[] volume) {
        System.out.println("valores array " + Arrays.toString(volume));
        Boolean updatePreviousRecords = this.updateRecordsLongSku(volume[9]);
        if (updatePreviousRecords) {
            Query query = entityManager.createNativeQuery("INSERT INTO producto_volumen (id_sku, numero_tag,\n" +
                    "                              largo,\n" +
                    "                              ancho,\n" +
                    "                              volumen,\n" +
                    "                              peso,\n" +
                    "                              volumen_peso,\n" +
                    "                              contenedor,\n" +
                    "                              altura,\n" +
                    "                              unidades_caja,\n" +
                    "                              status,\n" +
                    "                              cod_modelo)\n" +
                    "     VALUES (" + new BigDecimal(volume[9]) + ",\n" +
                    "             '" + volume[0] + "',\n" +
                    "             " + Float.parseFloat(volume[1]) + ",\n" +
                    "             " + Float.parseFloat(volume[2]) + ",\n" +
                    "             " + Float.parseFloat(volume[3]) + ",\n" +
                    "             " + Float.parseFloat(volume[4]) + ",\n" +
                    "             " + Float.parseFloat(volume[5]) + ",\n" +
                    "             '" + volume[6] + "',\n" +
                    "             " + Float.parseFloat(volume[7]) + ",\n" +
                    "             '" + volume[8] + "',\n" +
                    "             " + 1 + ",\n" +
                    "             '" + volume[10] + "')");
            return query.executeUpdate() > 0;
        } else {
            return false;
        }
    }

    @Override
    public Boolean insertProductVolumeByTagNumber(String[] volume, Boolean status) {
        int param = status ? 0 : 1;
        System.out.println("valores array " + Arrays.toString(volume));
        Query query = entityManager.createNativeQuery("INSERT INTO producto_volumen (id_sku, numero_tag,\n" +
                "                              largo,\n" +
                "                              ancho,\n" +
                "                              volumen,\n" +
                "                              peso,\n" +
                "                              volumen_peso,\n" +
                "                              contenedor,\n" +
                "                              altura,\n" +
                "                              unidades_caja,\n" +
                "                              status,\n" +
                "                              cod_modelo)\n" +
                "     VALUES (" + volume[9] + ",\n" +
                "             '" + volume[0] + "',\n" +
                "             " + Float.parseFloat(volume[1]) + ",\n" +
                "             " + Float.parseFloat(volume[2]) + ",\n" +
                "             " + Float.parseFloat(volume[3]) + ",\n" +
                "             " + Float.parseFloat(volume[4]) + ",\n" +
                "             " + Float.parseFloat(volume[5]) + ",\n" +
                "             '" + volume[6] + "',\n" +
                "             " + Float.parseFloat(volume[7]) + ",\n" +
                "             " + volume[8] + ",\n" +
                "             " + param + ",\n" +
                "             '" + volume[10] + "')");
        return query.executeUpdate() > 0;
    }

    @Override
    public BigDecimal compareWithLastProduct(String[] responseBody) {
        //responseBody[6]
        Query query = entityManager.createNativeQuery("SELECT COUNT(ultimo_volumen) ultimo_volumen\n" +
                "  FROM (SELECT (CASE WHEN COUNT (1) >= 1 THEN MAX(volumen) END) ultimo_volumen\n" +
                "          FROM producto_volumen\n" +
                "         WHERE id_sku = '780622026598' AND volumen = 0)");
        return (BigDecimal) query.getSingleResult();
    }

    @Override
    public List<Object> checkQuantityAndTagNumber(String tagNumber) {
        Query query = entityManager.createNativeQuery("SELECT DISTINCT qty,\n" +
                "                sku,\n" +
                "                cod_modelo,\n" +
                "                cod_color,\n" +
                "                modelo,\n" +
                "                color,\n" +
                "                tipo_sku,\n" +
                "                DECODE(tipo_sku, 'S', talla_numero, NULL) talla,\n" +
                "                container\n" +
                "  FROM asn_recibido a, producto_sku s, producto_s_atribut p\n" +
                " WHERE     a.carton_number = '" + tagNumber + "'\n" +
                "       AND a.sku = s.id_sku\n" +
                "       AND s.id_producto = p.id_producto");
        LOGGER.info("tag " + tagNumber);
        return (List<Object>) query.getResultList();
    }

    @Override
    public List<Object> requestHomeworkList(String tagNumber) {
        Query query = entityManager.createNativeQuery("SELECT cantidad,\n" +
                "       id_sku,\n" +
                "       tipo_sku,\n" +
                "       cod_modelo,\n" +
                "       cod_color,\n" +
                "       modelo,\n" +
                "       color,\n" +
                "       talla_numero,\n" +
                "       (codigo_barra || DIG_VER (codigo_barra)) codigo_barra,\n" +
                "       carton_number,\n" +
                "       clase,\n" +
                "       container\n" +
                "  FROM producto_sku s,\n" +
                "       producto_s_atribut p,\n" +
                "       producto b,\n" +
                "       asn_recibido a\n" +
                " WHERE     carton_number IN ('" + tagNumber + "')\n" +
                "       AND s.id_producto = p.id_producto\n" +
                "       AND s.id_producto = b.id_producto\n" +
                "       AND s.id_sku = a.sku");
        return (List<Object>) query.getResultList();
    }

/*
    @Override
    public List<Object> requestHomeworkList(String tagNumber) {
        Query query = entityManager.createNativeQuery("SELECT cantidad,\n" +
                "       id_sku,\n" +
                "       cod_modelo,\n" +
                "       cod_color,\n" +
                "       modelo,\n" +
                "       talla_numero,\n" +
                "       codigo_barra,\n" +
                "       container\n" +
                "  FROM producto_sku s, producto_s_atribut p, producto b, asn_recibido a\n" +
                " WHERE  carton_number IN ('" + tagNumber + "')\n" +
                "       AND s.id_producto = p.id_producto\n" +
                "       AND s.id_producto = b.id_producto\n" +
                "       AND s.id_sku = a.sku");
        return (List<Object>) query.getResultList();
    }
*/

    @Override
    public Boolean saveWeightIntoPmf(String weight, String sku) {
        Query query = entityManager.createNativeQuery("UPDATE pm_f\n" +
                "   SET wgt1 = " + Float.parseFloat(weight) + "\n" +
                " WHERE sku = " + new BigDecimal(sku));
        return query.executeUpdate() > 0;
    }

    @Override
    public List<Object> requestAuthentication(String user, String password) {
        Query query = entityManager.createNativeQuery("SELECT *\n" +
                "  FROM persona_cd\n" +
                " WHERE username = '" + user + "' AND codigorut = '" + password + "'");
        return (List<Object>) query.getResultList();
    }

    @Override
    public List<Object> requestDuplicatedProducts() {
        Query query = entityManager.createNativeQuery("SELECT DISTINCT p.id_sku,\n" +
                "       DECODE (numero_tag, NULL, 'UNIDAD', 'CAJA MASTER') tipo_producto,\n" +
                "       clase,\n" +
                "       largo,\n" +
                "       ancho,\n" +
                "       volumen,\n" +
                "       peso,\n" +
                "       volumen_peso,\n" +
                "       contenedor,\n" +
                "       create_date,\n" +
                "       unidades_caja,\n" +
                "       altura,\n" +
                "       numero_tag,\n" +
                "       status\n" +
                "  FROM producto_volumen p, producto_s_atribut a, producto_sku s\n" +
                " WHERE A.ID_PRODUCTO = S.ID_PRODUCTO AND P.ID_SKU = S.ID_SKU AND status = 0");
        return (List<Object>) query.getResultList();
    }

    @Override
    public List<Object> requestOriginalRecord(BigDecimal sku) {
        Query query = null;
        query = entityManager.createNativeQuery("SELECT id_sku,\n" +
                "       largo,\n" +
                "       ancho,\n" +
                "       volumen,\n" +
                "       peso,\n" +
                "       volumen_peso,\n" +
                "       contenedor,\n" +
                "       create_date,\n" +
                "       unidades_caja,\n" +
                "       altura,\n" +
                "       numero_tag\n" +
                "  FROM producto_volumen\n" +
                " WHERE id_sku = " + sku + " AND status = 1");
        return (List<Object>) query.getResultList();
    }

    @Override
    public Boolean requestDeclineNewVolume(BigDecimal sku) {
        Query query = entityManager.createNativeQuery("UPDATE producto_volumen\n" +
                "   SET status = 2\n" +
                " WHERE status = 0 AND id_sku = " + sku);
        return query.executeUpdate() > 0;
    }

    public Boolean updatePreviousRecords(String tag_number) {
        Query query = entityManager.createNativeQuery("UPDATE producto_volumen\n" +
                "   SET status = 2\n" +
                " WHERE status = 1 AND numero_tag = '" + tag_number + "'");
        return query.executeUpdate() > 0;
    }

    public Boolean updatePreviousRecordsBySku(String id_sku) {
        Query query = entityManager.createNativeQuery("UPDATE producto_volumen SET status = 2 WHERE status = 1 AND numero_tag IS NULL AND id_sku = " + Integer.parseInt(id_sku));
        return query.executeUpdate() > 0;
    }

    public Boolean updateRecordsLongSku(String sku) {
        Query query = entityManager.createNativeQuery("UPDATE producto_volumen SET status = 2 WHERE status = 1 AND id_sku = " + new BigDecimal(sku));
        return query.executeUpdate() > 0;
    }

    public static String dateFormater(String dateFromJSON, String expectedFormat, String oldFormat) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(oldFormat);
        Date date = null;
        String convertedDate = null;
        try {
            date = dateFormat.parse(dateFromJSON);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(expectedFormat);
            convertedDate = simpleDateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertedDate;
    }

    @Override
    public Boolean requestSaveNewVolume(String[] newVolume) {
        System.out.println("los datos para guardar: " + newVolume);
        BigDecimal sku = new BigDecimal(newVolume[0]);
        Query query = null;
        query = entityManager.createNativeQuery("UPDATE producto_volumen\n" +
                "   SET largo = " + Float.parseFloat(newVolume[3]) + ",\n" +
                "       ancho = " + Float.parseFloat(newVolume[4]) + ",\n" +
                "       volumen = " + Float.parseFloat(newVolume[5]) + ",\n" +
                "       peso = " + Float.parseFloat(newVolume[6]) + ",\n" +
                "       volumen_peso = " + Float.parseFloat(newVolume[7]) + ",\n" +
                "       contenedor = '" + newVolume[8] + "',\n" +
                "       create_date = TO_DATE(:create_date, 'DD-MM-YYYY HH24:MI:SS'),\n" +
                "       unidades_caja = '" + newVolume[10] + "',\n" +
                "       altura = " + Float.parseFloat(newVolume[11]) + "\n" +
                " WHERE status = 1 AND id_sku =" + sku).setParameter("create_date", newVolume[9]);
        this.declineOtherSkuBoxes(sku);
        return query.executeUpdate() > 0;
    }

    public Boolean declineOtherSkuBoxes(BigDecimal sku) {
        Query query = entityManager.createNativeQuery("UPDATE producto_volumen\n" +
                "   SET status = 2\n" +
                " WHERE id_sku = " + sku + " AND status != 1");
        return query.executeUpdate() > 0;
    }

    public String getCodModeloBySku(BigDecimal sku) {
        Query query = entityManager.createNativeQuery("SELECT DISTINCT TRIM (cod_modelo)\n" +
                "  FROM producto_sku s, producto_s_atribut p\n" +
                " WHERE s.id_producto = p.id_producto AND id_sku = " + sku);
        return (String) query.getSingleResult();
    }

    @Override
    public Boolean updateSkuWeightIntoPmf(BigDecimal sku, Float weight) {
        String cod_modelo = this.getCodModeloBySku(sku);
        System.out.println("El código modelo es: " + cod_modelo);
        Query query = entityManager.createNativeQuery("UPDATE pm_f\n" +
                "   SET wgt1 = " + weight + "\n" +
                " WHERE uom1 <> 'TAREA' AND sku_desc LIKE ('%" + cod_modelo + "%')");
        return query.executeUpdate() > 0;
    }

    @Override
    public Boolean updateHomeworkWeightIntoPmf(BigDecimal sku, Float weight) {
        String cod_modelo = this.getCodModeloBySku(sku);
        Query query = entityManager.createNativeQuery("UPDATE pm_f\n" +
                "   SET wgt1 = " + weight + "\n" +
                " WHERE uom1 = 'TAREA' AND sku_desc LIKE ('%" + cod_modelo + "%')");
        return query.executeUpdate() > 0;
    }

    @Override
    public Boolean insertProductVolumeByUnityFromMasterBox(String[] volume, Boolean status) {
        System.out.println("ARRAY: " + Arrays.toString(volume));
        int param = status ? 0 : 1;
        Query query = entityManager.createNativeQuery("INSERT INTO producto_volumen (id_sku,\n" +
                "                              largo,\n" +
                "                              ancho,\n" +
                "                              volumen,\n" +
                "                              peso,\n" +
                "                              volumen_peso,\n" +
                "                              contenedor,\n" +
                "                              altura,\n" +
                "                              status,\n" +
                "                              cod_modelo)\n" +
                "     VALUES (" + volume[0] + ",\n" +
                "             " + Float.parseFloat(volume[1]) + ",\n" +
                "             " + Float.parseFloat(volume[2]) + ",\n" +
                "             " + Float.parseFloat(volume[3]) + ",\n" +
                "             " + Float.parseFloat(volume[4]) + ",\n" +
                "             " + Float.parseFloat(volume[5]) + ",\n" +
                "             '" + volume[6] + "',\n" +
                "             " + Float.parseFloat(volume[7]) + ",\n" +
                "             " + param + ",\n" +
                "             '" + volume[8] + "')");
        return query.executeUpdate() > 0;
    }
}
