'use strict';
/**
 *
 * QUEDA PENDIENTE REVISAR LA LISTA DE TAREAS CUANDO SE INGRESA POR SKU.
 * DEBE FUNCIONAR IGUAL QUE CUANDO SE INGRESA POR Nº DE TAG?
 * 1. DEJE PENDIENTE POR TERMINAR LA VERIFICACION DE CANTIDADES POR TAREA AL BUSCAR POR SKU
 *
 */

let container;

moment.locale('es');

$("#modalConfirmacion").on("hidden.bs.modal", () => {
    $("#verificationTable, #currentRecordTable").html("");
});

const Spinner = () => {
    return (<p style={{
        marginBottom: 0,
        textAlign: 'center',
        color: '#fff'
    }}> Cargando... <img
        src="https://kitchencenter-production.s3.amazonaws.com/assets/spinner-38dac62eb9a19a9015d273a2457d2592145bc09bb2c35ead7b65274fb7513a7f.gif"
        style={{width: 20, marginBottom: 0, height: 20}} alt=""/></p>);
};

const SpinnerModal = () => {
    return (<p style={{
        marginBottom: 0,
        textAlign: 'center',
        color: '#fff'
    }}> Cargando... <img
        src="https://kitchencenter-production.s3.amazonaws.com/assets/spinner-38dac62eb9a19a9015d273a2457d2592145bc09bb2c35ead7b65274fb7513a7f.gif"
        style={{width: 60, height: 60, position: 'absolute', left: 0, right: 0, top: 0, bottom: 0, margin: 'auto'}}
        alt=""/></p>);
};

async function requestParcelCubeInfo(sku) {
    const response = await fetch(`http://localhost:8086/checkWeight?sku=${sku}`);
    return Promise.all([{
        status: response.status,
        response: await response.text()
    }]);
}

async function requestValidationByTagNumber(tagNumber) {
    const response = await fetch(`http://localhost:8086/checkQuantityAndTagNumber?tag_number=${tagNumber}`);
    return Promise.all([{
        status: response.status,
        response: await response.json()
    }]);
}

async function requestSKUValidation(sku) {
    const response = await fetch(`http://localhost:8086/checkSKU?sku=${sku}`);
    return Promise.all([{
        status: response.status,
        response: await response.json()
    }]);
}

async function asyncRequestTypeHomework(tagNumber) {
    const response = await fetch(`http://localhost:8086/requestTypeHomework?tag_number=${tagNumber}`);
    return Promise.all([{
        status: response.status,
        response: await response.json()
    }]);
}

async function saveVolume(volume, statusDuplicated = false) {
    const response = await fetch(`http://localhost:8086/insertProductVolume?volume=${volume}&status=${statusDuplicated}`);
    return Promise.all([{
        status: response.status,
        response: await response.json()
    }]);
}

async function saveVolumeByTagNumber(volume, statusDuplicated = false) {
    const response = await fetch(`http://localhost:8086/insertProductVolumeByTagNumber?volume=${volume}&status=${statusDuplicated}`);
    return Promise.all([{
        status: response.status,
        response: await response.json()
    }]);
}

async function saveVolumeByUnityFromMasterBox(volume, statusDuplicated = false) {
    const response = await fetch(`http://localhost:8086/insertProductVolumeByUnityFromMasterBox?volume=${volume}&status=${statusDuplicated}`);
    return Promise.resolve({
        status: response.status,
        response: await response.json()
    });
}

async function saveWeightIntoPmf(weight, sku) {
    const response = await fetch(`http://localhost:8086/saveWeightIntoPmf?weight=${weight}&sku=${sku}`);
    return Promise.all([{
        status: response.status,
        response: await response.json()
    }]);
}

async function checkRecordBySKU(sku) {
    const response = await fetch('http://localhost:8086/checkRecordBySKU?sku=' + sku.toString());
    return Promise.all([{
        status: response.status,
        response: await response.json()
    }]);
}

async function updateSkuWeightIntoPmf(sku, weight) {
    const response = await fetch(`http://localhost:8086/updateSkuWeightIntoPmf?sku=${sku}&weight=${weight}`);
    return Promise.all([{
        status: response.status,
        response: await response.json()
    }]);
}

async function updateHomeworkWeightIntoPmf(sku, weight) {
    const response = await fetch(`http://localhost:8086/updateHomeworkWeightIntoPmf?sku=${sku}&weight=${weight}`);
    return Promise.resolve({
        status: response.status,
        response: await response.json()
    })
}

async function checkRecordByUnitySKU(sku) {
    const response = await fetch('http://localhost:8086/checkRecordByUnitySKU?sku=' + sku);
    return Promise.all([{
        status: response.status,
        response: await response.json()
    }]);
}

async function requestSKUValidationFromMasterBox(sku) {
    const response = await fetch(`http://localhost:8086/requestSKUValidationFromMasterBox?sku=${sku}`);
    return Promise.resolve({
        status: response.status,
        response: await response.json()
    });
}

async function requestSKUModelCode(sku) {
    const response = await fetch(`http://localhost:8086/requestSKUModelCode?sku=${sku}`);
    return Promise.resolve({
        status: response.status,
        response: await response.json()
    });
}

async function requestPreviousRecordByTagNumber(tagNumber) {
    const response = await fetch(`http://localhost:8086/requestPreviousRecord?tagNumber=${tagNumber}`);
    return Promise.resolve({
        status: response.status,
        response: await response.json()
    });
}

async function saveNewRecord(array, isOnlySku = false) {
    const response = await fetch(`http://localhost:8086/saveNewRecord?volume=${array}&isOnlySku=${isOnlySku}`);
    return Promise.resolve({
        status: response.status,
        response: await response.json()
    });
}

async function requestPreviousRecordBySKU(sku) {
    const response = await fetch(`http://localhost:8086/requestPreviousRecordBySKU?sku=${sku}`);
    return Promise.resolve({
        status: response.status,
        response: await response.json()
    });
}

async function requestPreviousRecordBySKUAndTagNumber(sku) {
    const response = await fetch(`http://localhost:8086/requestPreviousRecordBySKUAndTagNumber?sku=${sku}`);
    return Promise.resolve({
        status: response.status,
        response: await response.json()
    });
}

async function saveNewRecordLongSku(volume) {
    console.log('volumen del volumen del volumen', volume);
    const response = await fetch(`http://localhost:8086/saveNewRecordLongSku?volume=${volume}`);
    return Promise.resolve({
        status: response.status,
        response: await response.json()
    });
}

class FormParcelCube extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            successAlert: false,
            errorAlert: false,
            warningAlert: false,
            alertMsg: '',
            spinner: false,
            spinnerValidate: false,
            spinnerSearch: false,
            spinnerSave: false,
            boxLength: '',
            boxWidth: '',
            boxHeight: '',
            boxVolume: '',
            boxWeight: '',
            boxVolumeWeight: '',
            boxSKU: '',
            boxItemsQuantity: '',
            boxContainer: '',
            tagNumber: '',
            productType: 0,
            productDetail: null,
            realBoxItemsQuantity: '',
            arrayHomework: [],
            toggleCheckbox: true,
            barCode: '',
            checkboxInputs: [],
            nonValid: [],
            duplicateSku: null,
            toggleInputTextType: false,
            modelCode: '',
            spinnerModal: false,
            tipoSKU: ''
        };
        this.boxItemsQuantity = null;
        this.array = [];
        this.nonValid = [];
        this.inputSearchBarCode = null;
        container = this;
    }

    componentDidMount() {
        localStorage.getItem('duplicateSku') !== null && this.setState({duplicateSku: localStorage.getItem('duplicateSku')});
    }

    validateBySKU = async () => {
        const {
            boxLength,
            boxWidth,
            boxHeight,
            boxVolume,
            boxWeight,
            boxVolumeWeight,
            boxContainer,
            boxSKU,
            modelCode
        } = this.state;
        if (parseFloat(boxLength) <= 0 || boxLength === "" || parseFloat(boxWidth) <= 0 || boxWidth === "" || parseFloat(boxHeight) <= 0 || boxHeight === "" || boxVolume === "" || parseFloat(boxWeight) <= 0 || boxWeight === "") {
            this.setState({
                errorAlert: true,
                alertMsg: 'Hay valores que están en 0 ó incompletos. Intente pesando una caja y asegúrese de llenar todos los campos.'
            });
            return;
        }
        if (!/^\d+$/.test(this.state.boxSKU)) {
            this.setState({
                errorAlert: true,
                alertMsg: 'El SKU no es válido o tiene un formato incorrecto.'
            });
            return;
        }
        this.setState({
            spinnerValidate: true,
            successAlert: false,
            errorAlert: false,
            alertMsg: ''
        });
        try {
            if (this.state.boxSKU.length > 6) {
                const result = await requestSKUValidation(this.state.boxSKU);
                const {status, response} = result[0];
                switch (status) {
                    case 200:
                        if (response.length > 0) {
                            const resp = response[0];
                            const obj = {
                                cantidad: resp[0],
                                sku: resp[1],
                                tipoSKU: resp[2],
                                codigoModelo: resp[3].trim(),
                                codigoColor: resp[4].trim(),
                                modelo: resp[5],
                                color: resp[6],
                                talla: resp[7]
                            };
                            this.setState({
                                modelCode: resp[3].trim()
                            });
                            console.log('obj sku', obj);
                            if (resp[2] === 'S') {
                                console.log('tipo solido');

                                const volumeArray = [resp[1], boxLength, boxWidth, boxVolume, boxWeight, boxVolumeWeight, boxContainer, boxHeight, resp[3].trim()];
                                //const responseCheck = await checkRecordByUnitySKU(resp[3]);
                                const responseCheck = await checkRecordBySKU(resp[1]);
                                console.log('validacion de existencia de sku POR SKU SÓLIDO', responseCheck);
                                const {status, response} = responseCheck[0];
                                if (status === 200) {
                                    if (response > 0) {
                                        this.setState({spinnerValidate: false});
                                        let modalConfirmacion = $("#modalConfirmacion");
                                        modalConfirmacion.modal("show");


                                        modalConfirmacion.on("shown.bs.modal", async () => {
                                            this.setState({spinnerModal: true});
                                            const responsePreviousRecord = await requestPreviousRecordBySKUAndTagNumber(resp[1]);
                                            console.log('responsePreviousRecord', responsePreviousRecord);
                                            const {status, response} = responsePreviousRecord;
                                            if (status === 200) {
                                                this.setState({spinnerModal: false});
                                                const data = response[0];

                                                console.log('data', data);

                                                let previousRecord = '<div className="widget">\n' +
                                                    '                                        <div className="widget-header">\n' +
                                                    '                                            <div className="alert alert-warning"><h4><strong>Volumetría previa</strong></h4></div>\n' +
                                                    '                                        </div>\n' +
                                                    '                                        <div className="widget-content">\n' +
                                                    '                                        <br/>\n' +
                                                    '                                       <div className="table-responsive">\n' +
                                                    '                                            <table className="table table-condensed table-bordered tablePreviousRecord" width="100%">\n' +
                                                    '                                                <thead>\n' +
                                                    '                                                <tr>\n' +
                                                    '                                                    <th>SKU</th>\n' +
                                                    '                                                    <th>LARGO</th>\n' +
                                                    '                                                    <th>ANCHO</th>\n' +
                                                    '                                                    <th>VOLUMEN</th>\n' +
                                                    '                                                    <th>PESO</th>\n' +
                                                    '                                                    <th>VOL. PESO</th>\n' +
                                                    '                                                    <th>CONTENEDOR</th>\n' +
                                                    '                                                    <th>F. CREACIÓN</th>\n' +
                                                    '                                                    <th>ALTURA</th>\n' +
                                                    '                                                    <th>COD. MODELO</th>\n' +
                                                    '                                                </tr>\n' +
                                                    '                                                </thead>\n' +
                                                    '                                                <tbody>\n' +
                                                    '                                                <tr>\n' +
                                                    '                                                    <td>' + data[0] + '</td>\n' +
                                                    '                                                    <td>' + data[1] + '</td>\n' +
                                                    '                                                    <td>' + data[2] + '</td>\n' +
                                                    '                                                    <td>' + data[3] + '</td>\n' +
                                                    '                                                    <td>' + data[4] + '</td>\n' +
                                                    '                                                    <td>' + data[5] + '</td>\n' +
                                                    '                                                    <td>' + data[6] + '</td>\n' +
                                                    '                                                    <td>' + moment(data[11]).format('LLL') + '</td>\n' +
                                                    '                                                    <td>' + data[13] + '</td>\n' +
                                                    '                                                    <td>' + data[16] + '</td>\n' +
                                                    '                                                </tr>\n' +
                                                    '                                                </tbody>\n' +
                                                    '                                            </table>\n' +
                                                    '                                            </div>\n' +
                                                    '                                        </div>\n' +
                                                    '                                    </div>';


                                                let currentRecord = '<div className="widget">\n' +
                                                    '                                        <div className="widget-header">\n' +
                                                    '                                            <div className="alert alert-info"><h4><strong>Volumetría actual</strong></h4></div>\n' +
                                                    '                                        </div>\n' +
                                                    '                                        <div className="widget-content">\n' +
                                                    '                                        <br/>\n' +
                                                    '                                            <div className="table-responsive">\n' +
                                                    '                                                <table\n' +
                                                    '                                                    className="table table-condensed table-bordered tableCurrentRecord" width="100%">\n' +
                                                    '                                                    <thead>\n' +
                                                    '                                                    <tr>\n' +
                                                    '                                                        <th>SKU</th>\n' +
                                                    '                                                        <th>LARGO</th>\n' +
                                                    '                                                        <th>ANCHO</th>\n' +
                                                    '                                                        <th>VOLUMEN</th>\n' +
                                                    '                                                        <th>PESO</th>\n' +
                                                    '                                                        <th>VOL. PESO</th>\n' +
                                                    '                                                        <th>CONTENEDOR</th>\n' +
                                                    '                                                        <th>F. CREACIÓN</th>\n' +
                                                    '                                                        <th>ALTURA</th>\n' +
                                                    '                                                        <th>COD. MODELO</th>\n' +
                                                    '                                                    </tr>\n' +
                                                    '                                                    </thead>\n' +
                                                    '                                                    <tbody>\n' +
                                                    '                                                    <tr>\n' +
                                                    '                                                        <td>' + boxSKU.substring(0, boxSKU.length - 1) + '</td>\n' +
                                                    '                                                        <td>' + boxLength + '</td>\n' +
                                                    '                                                        <td>' + boxWidth + '</td>\n' +
                                                    '                                                        <td>' + boxVolume + '</td>\n' +
                                                    '                                                        <td>' + boxWeight + '</td>\n' +
                                                    '                                                        <td>' + boxVolumeWeight + '</td>\n' +
                                                    '                                                        <td>' + boxContainer + '</td>\n' +
                                                    '                                                        <td>Actual</td>\n' +
                                                    '                                                        <td>' + boxHeight + '</td>\n' +
                                                    '                                                        <td>' + resp[3].trim() + '</td>\n' +
                                                    '                                                    </tr>\n' +
                                                    '                                                    </tbody>\n' +
                                                    '                                                </table>\n' +
                                                    '                                            </div>\n' +
                                                    '                                        </div>\n' +
                                                    '                                    </div>';

                                                $("#verificationTable").html(previousRecord);
                                                $("#currentRecordTable").html(currentRecord);
                                            }
                                        });


                                        /*  console.log('volumeArray', volumeArray);
                                          const res = await saveVolume(volumeArray, true);
                                          console.log('resultado guardar unidad repetida por sku', res);
                                          console.log('ESTE SKU YA FUE INGRESADO');
                                          const {status, response} = res[0];
                                          if (status === 200) {
                                              if (response) {
                                                  this.setState({
                                                      spinnerValidate: false,
                                                      warningAlert: true,
                                                      successAlert: false,
                                                      errorAlert: false,
                                                      alertMsg: 'Este SKU ya se encuentra ingresado, revisar duplicados.',
                                                      productDetail: obj,
                                                      realBoxItemsQuantity: resp[0],
                                                      arrayHomework: []
                                                  });
                                                  if (localStorage.getItem('duplicateSku') === null || localStorage.getItem('duplicateSku') === '') {
                                                      this.setState({
                                                          duplicateSku: 1
                                                      });
                                                  } else {
                                                      this.setState({
                                                          duplicateSku: '+1'
                                                      });
                                                  }
                                                  localStorage.setItem('duplicateSku', this.state.duplicateSku);
                                              }
                                          }*/
                                    } else {
                                        const res = await saveVolume(volumeArray);
                                        console.log('resultado de guardar primer sku', res);
                                        const {status, response} = res[0];
                                        if (status === 200) {
                                            if (response) {
                                                //const result = await saveWeightIntoPmf(boxWeight, boxSKU);
                                                const result = await updateSkuWeightIntoPmf(resp[1], boxWeight);
                                                console.log('resultado sku pmf', result);
                                                const {status, response} = result[0];
                                                if (status === 200) {
                                                    this.setState({
                                                        spinnerValidate: false,
                                                        productDetail: obj,
                                                        realBoxItemsQuantity: resp[0],
                                                        arrayHomework: [],
                                                        errorAlert: false,
                                                        successAlert: true,
                                                        warningAlert: false,
                                                        alertMsg: 'El SKU es válido y ha sido guardado.'
                                                    });
                                                }
                                            }
                                        }
                                    }
                                }
                                /* this.setState({
                                     boxLength: '',
                                     boxWidth: '',
                                     boxHeight: '',
                                     boxVolume: '',
                                     boxWeight: '',
                                     boxVolumeWeight: '',
                                     boxContainer: '',
                                     boxSKU: '',
                                     boxItemsQuantity: '',
                                     realBoxItemsQuantity: ''
                                 });*/
                            } else {
                                /**
                                 * CUANDO NO ES SOLIDO PERO ES UNIDAD
                                 * */
                                /*    this.setState({
                                        productDetail: ''
                                    });
                                    let sum = 0,
                                        array = [],
                                        obj = {};
                                    console.log('tipo tarea');
                                    if (status === 200) {
                                        response.map((item, index) => {
                                            obj = {
                                                cantidad: item[0],
                                                sku: item[1],
                                                tipoSKU: item[2],
                                                codigoModelo: item[3],
                                                codigoColor: item[4],
                                                modelo: item[5],
                                                color: item[6],
                                                tallaNumero: item[7],
                                                codigoBarra: item[8]
                                            };
                                            array.push(obj);
                                            sum += parseInt(item[0]);
                                            this.array[index] = 0;
                                        });
                                        if (sum !== parseInt(this.state.boxItemsQuantity)) {
                                            this.setState({
                                                spinnerValidate: false,
                                                realBoxItemsQuantity: sum,
                                                errorAlert: true,
                                                alertMsg: 'No coinciden las unidades caja'
                                            });
                                            this.refs.boxItemsQuantity.focus();
                                            return;
                                        }
                                        this.setState({
                                            spinnerValidate: false,
                                            realBoxItemsQuantity: sum,
                                            arrayHomework: array,
                                            checkboxInputs: this.array
                                        });
                                    }*/
                            }
                        } else {
                            this.setState({
                                successAlert: false,
                                errorAlert: true,
                                spinnerValidate: false,
                                alertMsg: 'El SKU no existe.'
                            });
                        }
                        break;
                    case 500:
                        if (response.message === "could not extract ResultSet; SQL [n/a]; nested exception is org.hibernate.exception.SQLGrammarException: could not extract ResultSet")
                            this.setState({
                                successAlert: false,
                                errorAlert: true,
                                spinnerValidate: false,
                                alertMsg: 'Ocurrió un error al intentar hacer la consulta en la base de datos. Intente de nuevo o consulte con especialista.'
                            });
                        break;
                }
            } else {


                //tengo pendiente hacer la consulta aca del sku de unidad caja master para que me traiga el codigo modelo

                const result = await requestSKUValidationFromMasterBox(this.state.boxSKU);
                const {status, response} = result;
                console.log('this.state.modelCode', this.state.modelCode);
                this.setState({
                    spinnerValidate: true
                });
                if (status === 200) {
                    if (!response) {
                        const result = await requestSKUModelCode(this.state.boxSKU);
                        const {status, response} = result;
                        const modelCodeResponse = response;
                        if (status === 200) {
                            const volumeArray = [this.state.boxSKU, boxLength, boxWidth, boxVolume, boxWeight, boxVolumeWeight, null, boxHeight, modelCodeResponse];
                            const result = await saveVolumeByUnityFromMasterBox(volumeArray);
                            console.log('result', result);
                            const {status, response} = result;
                            if (status === 200) {
                                if (response) {
                                    this.setState({
                                        spinnerValidate: false,
                                        arrayHomework: [],
                                        errorAlert: false,
                                        successAlert: true,
                                        warningAlert: false,
                                        alertMsg: 'El SKU es válido y ha sido guardado.'
                                    });
                                }
                            }
                        }
                    } else {
                        const result = await requestSKUModelCode(this.state.boxSKU);
                        const {status, response} = result;
                        const modelCodeResponse = response;

                        console.log('modelCodeResponse', modelCodeResponse);
                        if (status === 200) {
                            const volumeArray = [this.state.boxSKU, boxLength, boxWidth, boxVolume, boxWeight, boxVolumeWeight, null, boxHeight, modelCodeResponse];

                            this.setState({spinnerValidate: false});
                            let modalConfirmacion = $("#modalConfirmacion");
                            modalConfirmacion.modal("show");


                            modalConfirmacion.on("shown.bs.modal", async () => {
                                this.setState({spinnerModal: true});
                                const responsePreviousRecord = await requestPreviousRecordBySKU(boxSKU);
                                console.log('responsePreviousRecord', responsePreviousRecord);
                                const {status, response} = responsePreviousRecord;
                                if (status === 200) {
                                    this.setState({spinnerModal: false});
                                    const data = response[0];
                                    let previousRecord = '<div className="widget">\n' +
                                        '                                        <div className="widget-header">\n' +
                                        '                                            <div className="alert alert-warning"><h4><strong>Volumetría previa</strong></h4></div>\n' +
                                        '                                        </div>\n' +
                                        '                                        <div className="widget-content">\n' +
                                        '                                        <br/>\n' +
                                        '                                       <div className="table-responsive">\n' +
                                        '                                            <table className="table table-condensed table-bordered tablePreviousRecord" width="100%">\n' +
                                        '                                                <thead>\n' +
                                        '                                                <tr>\n' +
                                        '                                                    <th>SKU</th>\n' +
                                        '                                                    <th>LARGO</th>\n' +
                                        '                                                    <th>ANCHO</th>\n' +
                                        '                                                    <th>VOLUMEN</th>\n' +
                                        '                                                    <th>PESO</th>\n' +
                                        '                                                    <th>VOL. PESO</th>\n' +
                                        '                                                    <th>CONTENEDOR</th>\n' +
                                        '                                                    <th>F. CREACIÓN</th>\n' +
                                        '                                                    <th>ALTURA</th>\n' +
                                        '                                                    <th>COD. MODELO</th>\n' +
                                        '                                                </tr>\n' +
                                        '                                                </thead>\n' +
                                        '                                                <tbody>\n' +
                                        '                                                <tr>\n' +
                                        '                                                    <td>' + data[0] + '</td>\n' +
                                        '                                                    <td>' + data[1] + '</td>\n' +
                                        '                                                    <td>' + data[2] + '</td>\n' +
                                        '                                                    <td>' + data[3] + '</td>\n' +
                                        '                                                    <td>' + data[4] + '</td>\n' +
                                        '                                                    <td>' + data[5] + '</td>\n' +
                                        '                                                    <td>' + data[6] + '</td>\n' +
                                        '                                                    <td>' + moment(data[11]).format('LLL') + '</td>\n' +
                                        '                                                    <td>' + data[13] + '</td>\n' +
                                        '                                                    <td>' + data[16] + '</td>\n' +
                                        '                                                </tr>\n' +
                                        '                                                </tbody>\n' +
                                        '                                            </table>\n' +
                                        '                                            </div>\n' +
                                        '                                        </div>\n' +
                                        '                                    </div>';


                                    let currentRecord = ' <div className="widget">\n' +
                                        '                                        <div className="widget-header">\n' +
                                        '                                            <div className="alert alert-info"><h4><strong>Volumetría actual</strong></h4></div>\n' +
                                        '                                        </div>\n' +
                                        '                                        <div className="widget-content">\n' +
                                        '                                        <br/>\n' +
                                        '                                            <div className="table-responsive">\n' +
                                        '                                                <table\n' +
                                        '                                                    className="table table-condensed table-bordered tableCurrentRecord" width="100%">\n' +
                                        '                                                    <thead>\n' +
                                        '                                                    <tr>\n' +
                                        '                                                        <th>SKU</th>\n' +
                                        '                                                        <th>LARGO</th>\n' +
                                        '                                                        <th>ANCHO</th>\n' +
                                        '                                                        <th>VOLUMEN</th>\n' +
                                        '                                                        <th>PESO</th>\n' +
                                        '                                                        <th>VOL. PESO</th>\n' +
                                        '                                                        <th>CONTENEDOR</th>\n' +
                                        '                                                        <th>F. CREACIÓN</th>\n' +
                                        '                                                        <th>ALTURA</th>\n' +
                                        '                                                        <th>COD. MODELO</th>\n' +
                                        '                                                    </tr>\n' +
                                        '                                                    </thead>\n' +
                                        '                                                    <tbody>\n' +
                                        '                                                    <tr>\n' +
                                        '                                                        <td>' + this.state.boxSKU + '</td>\n' +
                                        '                                                        <td>' + this.state.boxLength + '</td>\n' +
                                        '                                                        <td>' + this.state.boxWidth + '</td>\n' +
                                        '                                                        <td>' + this.state.boxVolume + '</td>\n' +
                                        '                                                        <td>' + this.state.boxWeight + '</td>\n' +
                                        '                                                        <td>' + this.state.boxVolumeWeight + '</td>\n' +
                                        '                                                        <td>' + this.state.boxContainer + '</td>\n' +
                                        '                                                        <td>Actual</td>\n' +
                                        '                                                        <td>' + this.state.boxHeight + '</td>\n' +
                                        '                                                        <td>' + modelCodeResponse + '</td>\n' +
                                        '                                                    </tr>\n' +
                                        '                                                    </tbody>\n' +
                                        '                                                </table>\n' +
                                        '                                            </div>\n' +
                                        '                                        </div>\n' +
                                        '                                    </div>';

                                    $("#verificationTable").html(previousRecord);
                                    $("#currentRecordTable").html(currentRecord);
                                }
                            });

                            /*    const result = await saveVolumeByUnityFromMasterBox(volumeArray, true);
                                console.log('result', result);
                                const {status, response} = result;
                                if (status === 200) {
                                    if (response) {
                                        this.setState({
                                            spinnerValidate: false,
                                            arrayHomework: [],
                                            errorAlert: false,
                                            successAlert: false,
                                            warningAlert: true,
                                            alertMsg: 'Este SKU ya se encuentra ingresado, revisar duplicados.'
                                        });
                                    }
                                }*/
                        }
                    }
                    /* this.setState({
                         boxLength: '',
                         boxWidth: '',
                         boxHeight: '',
                         boxVolume: '',
                         boxWeight: '',
                         boxVolumeWeight: '',
                         boxContainer: '',
                         boxSKU: '',
                         boxItemsQuantity: '',
                         realBoxItemsQuantity: ''
                     });*/
                }
            }
        } catch (e) {
            console.log('atrapanto errores', e);
        }
    };

    validateByTagNumber = async () => {
        const {
            boxLength,
            boxWidth,
            boxHeight,
            boxVolume,
            boxWeight,
            boxVolumeWeight,
            boxContainer,
            tagNumber,
            modelCode,
            boxItemsQuantity
        } = this.state;
        if (parseFloat(boxLength) <= 0 || boxLength === "" || parseFloat(boxWidth) <= 0 || boxWidth === "" || parseFloat(boxHeight) <= 0 || boxHeight === "" || boxVolume === "" || parseFloat(boxWeight) <= 0 || boxWeight === "") {
            this.setState({
                errorAlert: true,
                alertMsg: 'Hay valores que están en 0 ó incompletos. Intente pesando una caja y asegúrese de llenar todos los campos.'
            });
            return;
        }
        this.setState({
            spinnerValidate: true,
            successAlert: false,
            errorAlert: false,
            alertMsg: ''
        });
        try {
            const result = await requestValidationByTagNumber(this.state.tagNumber);
            const {status, response} = result[0];
            console.log('response by tag number', result);
            if (response.length > 0) {
                const resp = response[0];
                if (status === 200) {
                    const obj = {
                        cantidad: resp[0],
                        sku: resp[1],
                        codigoModelo: resp[2].trim(),
                        codigoColor: resp[3],
                        modelo: resp[4],
                        color: resp[5],
                        tipoSKU: resp[6],
                        talla: resp[7],
                        container: resp[8]
                    };
                    this.setState({
                        modelCode: resp[2].trim(),
                        tipoSKU: resp[6]
                    });
                    if (resp[6] === 'S') {

                        console.log('tipo solido');

                        /*   if (parseInt(this.state.boxItemsQuantity) !== parseInt(resp[0])) {
                               this.setState({
                                   spinnerValidate: false,
                                   realBoxItemsQuantity: resp[0],
                                   errorAlert: true,
                                   alertMsg: 'No coinciden las unidades caja',
                                   boxContainer: resp[8]
                               });
                               this.refs.boxItemsQuantity.focus();
                               return;
                           }*/
                        this.setState({
                            boxContainer: resp[8],
                            realBoxItemsQuantity: resp[0],
                            boxSKU: resp[1]
                        });
                        const volumeArray = [tagNumber, boxLength, boxWidth, boxVolume, boxWeight, boxVolumeWeight, this.state.boxContainer, boxHeight, resp[0], parseInt(obj.sku), this.state.modelCode];
                        const responseCheck = await checkRecordBySKU(parseInt(obj.sku));
                        console.log('validacion de existencia de sku', responseCheck);
                        const {status, response} = responseCheck[0];
                        if (status === 200) {
                            if (response > 0) {
                                this.setState({spinnerValidate: false});
                                let modalConfirmacion = $("#modalConfirmacion");
                                modalConfirmacion.modal("show");


                                modalConfirmacion.on("shown.bs.modal", async () => {
                                    this.setState({spinnerModal: true});
                                    const responsePreviousRecord = await requestPreviousRecordByTagNumber(tagNumber);
                                    console.log('responsePreviousRecord', responsePreviousRecord);
                                    const {status, response} = responsePreviousRecord;
                                    if (status === 200) {
                                        this.setState({spinnerModal: false});
                                        const data = response[0];
                                        let previousRecord = '<div className="widget">\n' +
                                            '                                        <div className="widget-header">\n' +
                                            '                                            <div className="alert alert-warning"><h4><strong>Volumetría previa</strong></h4></div>\n' +
                                            '                                        </div>\n' +
                                            '                                        <div className="widget-content">\n' +
                                            '                                        <br/>\n' +
                                            '                                       <div className="table-responsive">\n' +
                                            '                                            <table className="table table-condensed table-bordered tablePreviousRecord" width="100%">\n' +
                                            '                                                <thead>\n' +
                                            '                                                <tr>\n' +
                                            '                                                    <th>SKU</th>\n' +
                                            '                                                    <th>LARGO</th>\n' +
                                            '                                                    <th>ANCHO</th>\n' +
                                            '                                                    <th>VOLUMEN</th>\n' +
                                            '                                                    <th>PESO</th>\n' +
                                            '                                                    <th>VOL. PESO</th>\n' +
                                            '                                                    <th>CONTENEDOR</th>\n' +
                                            '                                                    <th>F. CREACIÓN</th>\n' +
                                            '                                                    <th>UNID. CAJA</th>\n' +
                                            '                                                    <th>ALTURA</th>\n' +
                                            '                                                    <th>Nº TAG</th>\n' +
                                            '                                                    <th>COD. MODELO</th>\n' +
                                            '                                                </tr>\n' +
                                            '                                                </thead>\n' +
                                            '                                                <tbody>\n' +
                                            '                                                <tr>\n' +
                                            '                                                    <td>' + data[0] + '</td>\n' +
                                            '                                                    <td>' + data[1] + '</td>\n' +
                                            '                                                    <td>' + data[2] + '</td>\n' +
                                            '                                                    <td>' + data[3] + '</td>\n' +
                                            '                                                    <td>' + data[4] + '</td>\n' +
                                            '                                                    <td>' + data[5] + '</td>\n' +
                                            '                                                    <td>' + data[6] + '</td>\n' +
                                            '                                                    <td>' + moment(data[11]).format('LLL') + '</td>\n' +
                                            '                                                    <td>' + data[12] + '</td>\n' +
                                            '                                                    <td>' + data[13] + '</td>\n' +
                                            '                                                    <td>' + data[14] + '</td>\n' +
                                            '                                                    <td>' + data[16] + '</td>\n' +
                                            '                                                </tr>\n' +
                                            '                                                </tbody>\n' +
                                            '                                            </table>\n' +
                                            '                                            </div>\n' +
                                            '                                        </div>\n' +
                                            '                                    </div>';


                                        let currentRecord = ' <div className="widget">\n' +
                                            '                                        <div className="widget-header">\n' +
                                            '                                            <div className="alert alert-info"><h4><strong>Volumetría actual</strong></h4></div>\n' +
                                            '                                        </div>\n' +
                                            '                                        <div className="widget-content">\n' +
                                            '                                        <br/>\n' +
                                            '                                            <div className="table-responsive">\n' +
                                            '                                                <table\n' +
                                            '                                                    className="table table-condensed table-bordered tableCurrentRecord" width="100%">\n' +
                                            '                                                    <thead>\n' +
                                            '                                                    <tr>\n' +
                                            '                                                        <th>SKU</th>\n' +
                                            '                                                        <th>LARGO</th>\n' +
                                            '                                                        <th>ANCHO</th>\n' +
                                            '                                                        <th>VOLUMEN</th>\n' +
                                            '                                                        <th>PESO</th>\n' +
                                            '                                                        <th>VOL. PESO</th>\n' +
                                            '                                                        <th>CONTENEDOR</th>\n' +
                                            '                                                        <th>F. CREACIÓN</th>\n' +
                                            '                                                        <th>UNID. CAJA</th>\n' +
                                            '                                                        <th>ALTURA</th>\n' +
                                            '                                                        <th>Nº TAG</th>\n' +
                                            '                                                        <th>COD. MODELO</th>\n' +
                                            '                                                    </tr>\n' +
                                            '                                                    </thead>\n' +
                                            '                                                    <tbody>\n' +
                                            '                                                    <tr>\n' +
                                            '                                                        <td>' + obj.sku + '</td>\n' +
                                            '                                                        <td>' + this.state.boxLength + '</td>\n' +
                                            '                                                        <td>' + this.state.boxWidth + '</td>\n' +
                                            '                                                        <td>' + this.state.boxVolume + '</td>\n' +
                                            '                                                        <td>' + this.state.boxWeight + '</td>\n' +
                                            '                                                        <td>' + this.state.boxVolumeWeight + '</td>\n' +
                                            '                                                        <td>' + this.state.boxContainer + '</td>\n' +
                                            '                                                        <td>Actual</td>\n' +
                                            '                                                        <td>' + resp[0] + '</td>\n' +
                                            '                                                        <td>' + this.state.boxHeight + '</td>\n' +
                                            '                                                        <td>' + this.state.tagNumber + '</td>\n' +
                                            '                                                        <td>' + this.state.modelCode + '</td>\n' +
                                            '                                                    </tr>\n' +
                                            '                                                    </tbody>\n' +
                                            '                                                </table>\n' +
                                            '                                            </div>\n' +
                                            '                                        </div>\n' +
                                            '                                    </div>';

                                        $("#verificationTable").html(previousRecord);
                                        $("#currentRecordTable").html(currentRecord);
                                    }
                                });


                                // const res = await saveVolumeByTagNumber(volumeArray, true);
                                // const {status, response} = res[0];
                                // console.log('resultado guardar solido repetido tag number', res);
                                // if (status === 200) {
                                //     if (response) {
                                //         console.log('ESTE SKU YA FUE INGRESADO');
                                //         this.setState({
                                //             spinnerValidate: false,
                                //             warningAlert: true,
                                //             successAlert: false,
                                //             errorAlert: false,
                                //             alertMsg: 'Este SKU ya se encuentra registrado, revisar duplicados.',
                                //             productDetail: obj,
                                //             realBoxItemsQuantity: resp[0],
                                //             boxContainer: resp[8],
                                //         });
                                //         if (localStorage.getItem('duplicateSku') === null || localStorage.getItem('duplicateSku') === '') {
                                //             this.setState({
                                //                 duplicateSku: '1'
                                //             });
                                //         } else {
                                //             /* this.setState({
                                //                  duplicateSku: parseInt(localStorage.getItem('duplicateSku')) + 1
                                //              });*/
                                //             this.setState({
                                //                 duplicateSku: '+1'
                                //             });
                                //         }
                                //         localStorage.setItem('duplicateSku', this.state.duplicateSku);
                                //     }
                                // }
                            } else {
                                const res = await saveVolumeByTagNumber(volumeArray);
                                console.log('respuesta validar tag number', res);
                                const {status, response} = res[0];
                                if (status === 200) {
                                    if (response) {
                                        this.setState({
                                            spinnerValidate: false,
                                            warningAlert: false,
                                            successAlert: true,
                                            errorAlert: false,
                                            alertMsg: 'El SKU es válido y ha sido guardado.',
                                            productDetail: obj,
                                            realBoxItemsQuantity: resp[0],
                                            boxContainer: resp[8],
                                            arrayHomework: []
                                        });
                                    }
                                }
                            }
                        }
                        /* this.setState({
                             boxLength: '',
                             boxWidth: '',
                             boxHeight: '',
                             boxVolume: '',
                             boxWeight: '',
                             boxVolumeWeight: '',
                             tagNumber: '',
                         });*/
                    } else {
                        console.log('tipo tarea');
                        this.requestTypeHomework(this.state.tagNumber);
                    }
                }
            } else {
                if (status === 200) {
                    this.setState({
                        spinnerValidate: false,
                        productDetail: null,
                        errorAlert: true,
                        alertMsg: 'No existe el Nº de tag en la BD'
                    });
                }
            }
        } catch (e) {
            console.log('atrapando el error by tag number', e);
        }
    };

    handleChangeInputCheckbox = (index, value) => {
        this.state.checkboxInputs[index] = value;
        this.setState({
            checkboxInputs: this.state.checkboxInputs
        });
    };

    requestTypeHomework = async tagNumber => {
        try {
            let duplicateSku = await localStorage.getItem('duplicateSku');
            const {
                boxLength,
                boxWidth,
                boxHeight,
                boxVolume,
                boxWeight,
                boxVolumeWeight,
                boxContainer,
                realBoxItemsQuantity
            } = this.state;
            const result = await asyncRequestTypeHomework(tagNumber);
            const {status, response} = result[0];

            let sum = 0,
                array = [],
                container = '',
                obj = {};

            if (status === 200) {
                if (response.length > 0) {
                    response.map((item, index) => {
                        console.log('items', item);
                        obj = {
                            cantidad: item[0],
                            sku: item[1],
                            tipoSKU: item[2],
                            codigoModelo: item[3],
                            codigoColor: item[4],
                            modelo: item[5],
                            color: item[6],
                            tallaNumero: item[7],
                            codigoBarra: item[8],
                            cartonNumber: item[9],
                            clase: item[10],
                            container: item[11]
                        };
                        container = item[11];
                        array.push(obj);
                        sum += parseInt(item[0]);
                        this.array[index] = 0;
                        this.nonValid[index] = false;
                    });
                    /*               if (sum !== parseInt(this.state.boxItemsQuantity)) {
                                       this.setState({
                                           spinnerValidate: false,
                                           realBoxItemsQuantity: sum,
                                           boxContainer: container,
                                           errorAlert: true,
                                           alertMsg: 'No coinciden las unidades caja'
                                       });
                                       this.boxItemsQuantity.focus();
                                       return;
                                   }*/
                    this.setState({
                        boxContainer: container,
                        realBoxItemsQuantity: sum,
                        arrayHomework: array
                    });
                    console.log('numero de contenedor', this.state.boxContainer);
                    console.log('unidades caja', sum);
                    if (!this.state.toggleCheckbox) {
                        this.setState({spinnerValidate: true});
                        const responseCheck = await checkRecordBySKU(parseInt(array[0].sku));
                        console.log('validar existencia sku TAREA', responseCheck);
                        console.log('traer contenedor', container);
                        const {status, response} = responseCheck[0];
                        if (status === 200) {
                            const volumeArray = [tagNumber, boxLength, boxWidth, boxVolume, boxWeight, boxVolumeWeight, array[0].container, boxHeight, sum, array[0].sku, array[0].codigoModelo];
                            if (response > 0) {
                                let modalConfirmacion = $("#modalConfirmacion");
                                modalConfirmacion.modal("show");

                                modalConfirmacion.on("shown.bs.modal", async () => {
                                    this.setState({
                                        spinnerModal: true,
                                        spinnerValidate: false
                                    });
                                    const responsePreviousRecord = await requestPreviousRecordByTagNumber(tagNumber);
                                    console.log('responsePreviousRecord', responsePreviousRecord);
                                    const {status, response} = responsePreviousRecord;
                                    if (status === 200) {
                                        this.setState({spinnerModal: false});
                                        const data = response[0];
                                        let previousRecord = '<div className="widget">\n' +
                                            '                                        <div className="widget-header">\n' +
                                            '                                            <div className="alert alert-warning"><h4><strong>Volumetría previa</strong></h4></div>\n' +
                                            '                                        </div>\n' +
                                            '                                        <div className="widget-content">\n' +
                                            '                                        <br/>\n' +
                                            '                                       <div className="table-responsive">\n' +
                                            '                                            <table className="table table-condensed table-bordered tablePreviousRecord" width="100%">\n' +
                                            '                                                <thead>\n' +
                                            '                                                <tr>\n' +
                                            '                                                    <th>SKU</th>\n' +
                                            '                                                    <th>LARGO</th>\n' +
                                            '                                                    <th>ANCHO</th>\n' +
                                            '                                                    <th>VOLUMEN</th>\n' +
                                            '                                                    <th>PESO</th>\n' +
                                            '                                                    <th>VOL. PESO</th>\n' +
                                            '                                                    <th>CONTENEDOR</th>\n' +
                                            '                                                    <th>F. CREACIÓN</th>\n' +
                                            '                                                    <th>UNID. CAJA</th>\n' +
                                            '                                                    <th>ALTURA</th>\n' +
                                            '                                                    <th>Nº TAG</th>\n' +
                                            '                                                    <th>COD. MODELO</th>\n' +
                                            '                                                </tr>\n' +
                                            '                                                </thead>\n' +
                                            '                                                <tbody>\n' +
                                            '                                                <tr>\n' +
                                            '                                                    <td>' + data[0] + '</td>\n' +
                                            '                                                    <td>' + data[1] + '</td>\n' +
                                            '                                                    <td>' + data[2] + '</td>\n' +
                                            '                                                    <td>' + data[3] + '</td>\n' +
                                            '                                                    <td>' + data[4] + '</td>\n' +
                                            '                                                    <td>' + data[5] + '</td>\n' +
                                            '                                                    <td>' + data[6] + '</td>\n' +
                                            '                                                    <td>' + moment(data[11]).format('LLL') + '</td>\n' +
                                            '                                                    <td>' + data[12] + '</td>\n' +
                                            '                                                    <td>' + data[13] + '</td>\n' +
                                            '                                                    <td>' + data[14] + '</td>\n' +
                                            '                                                    <td>' + data[16] + '</td>\n' +
                                            '                                                </tr>\n' +
                                            '                                                </tbody>\n' +
                                            '                                            </table>\n' +
                                            '                                            </div>\n' +
                                            '                                        </div>\n' +
                                            '                                    </div>';


                                        let currentRecord = ' <div className="widget">\n' +
                                            '                                        <div className="widget-header">\n' +
                                            '                                            <div className="alert alert-info"><h4><strong>Volumetría actual</strong></h4></div>\n' +
                                            '                                        </div>\n' +
                                            '                                        <div className="widget-content">\n' +
                                            '                                        <br/>\n' +
                                            '                                            <div className="table-responsive">\n' +
                                            '                                                <table\n' +
                                            '                                                    className="table table-condensed table-bordered tableCurrentRecord" width="100%">\n' +
                                            '                                                    <thead>\n' +
                                            '                                                    <tr>\n' +
                                            '                                                        <th>SKU</th>\n' +
                                            '                                                        <th>LARGO</th>\n' +
                                            '                                                        <th>ANCHO</th>\n' +
                                            '                                                        <th>VOLUMEN</th>\n' +
                                            '                                                        <th>PESO</th>\n' +
                                            '                                                        <th>VOL. PESO</th>\n' +
                                            '                                                        <th>CONTENEDOR</th>\n' +
                                            '                                                        <th>F. CREACIÓN</th>\n' +
                                            '                                                        <th>UNID. CAJA</th>\n' +
                                            '                                                        <th>ALTURA</th>\n' +
                                            '                                                        <th>Nº TAG</th>\n' +
                                            '                                                        <th>COD. MODELO</th>\n' +
                                            '                                                    </tr>\n' +
                                            '                                                    </thead>\n' +
                                            '                                                    <tbody>\n' +
                                            '                                                    <tr>\n' +
                                            '                                                        <td>' + array[0].sku + '</td>\n' +
                                            '                                                        <td>' + boxLength + '</td>\n' +
                                            '                                                        <td>' + boxWidth + '</td>\n' +
                                            '                                                        <td>' + boxVolume + '</td>\n' +
                                            '                                                        <td>' + boxWeight + '</td>\n' +
                                            '                                                        <td>' + boxVolumeWeight + '</td>\n' +
                                            '                                                        <td>' + array[0].container + '</td>\n' +
                                            '                                                        <td>Actual</td>\n' +
                                            '                                                        <td>' + sum + '</td>\n' +
                                            '                                                        <td>' + boxHeight + '</td>\n' +
                                            '                                                        <td>' + tagNumber + '</td>\n' +
                                            '                                                        <td>' + array[0].codigoModelo + '</td>\n' +
                                            '                                                    </tr>\n' +
                                            '                                                    </tbody>\n' +
                                            '                                                </table>\n' +
                                            '                                            </div>\n' +
                                            '                                        </div>\n' +
                                            '                                    </div>';

                                        $("#verificationTable").html(previousRecord);
                                        $("#currentRecordTable").html(currentRecord);
                                    }
                                });
                                /*  const res = await saveVolumeByTagNumber(volumeArray, true);
                                  const {status, response} = res[0];
                                  console.log('resultado guardar tarea repetida', res);
                                  if (status === 200) {
                                      if (response) {
                                          this.setState({
                                              spinnerValidate: false,
                                              warningAlert: true,
                                              alertMsg: 'Este SKU ya se encuentra registrado, revisar duplicados.',
                                              realBoxItemsQuantity: sum,
                                              boxContainer: container,
                                              arrayHomework: array,
                                              checkboxInputs: this.array,
                                              productDetail: null
                                          });
                                          if (localStorage.getItem('duplicateSku') === null || localStorage.getItem('duplicateSku') === '') {
                                              this.setState({
                                                  duplicateSku: '1'
                                              });
                                          } else {
                                              this.setState({
                                                  duplicateSku: '+1'
                                              });
                                          }
                                          localStorage.setItem('duplicateSku', this.state.duplicateSku);
                                      }
                                  }*/
                            } else {
                                const res = await saveVolumeByTagNumber(volumeArray);
                                const {status, response} = res[0];
                                if (status === 200) {
                                    if (response) {
                                        this.setState({
                                            spinnerValidate: false,
                                            realBoxItemsQuantity: sum,
                                            boxContainer: container,
                                            arrayHomework: array,
                                            checkboxInputs: this.array,
                                            productDetail: null
                                        });

                                        console.log('resultado array final', array);

                                        if (array[0].clase === 'CALZADO') {
                                            const result = await updateHomeworkWeightIntoPmf(array[0].sku, boxWeight);
                                            console.log('resultado de pmf', result);
                                        }
                                    }
                                }
                            }
                        }
                        /*  this.setState({
                              productDetail: null,
                              boxLength: '',
                              boxWidth: '',
                              boxHeight: '',
                              boxVolume: '',
                              boxWeight: '',
                              boxVolumeWeight: '',
                              tagNumber: '',
                          });*/
                    } else {
                        this.setState({
                            spinnerValidate: false,
                            realBoxItemsQuantity: sum,
                            arrayHomework: array,
                            checkboxInputs: this.array,
                            productDetail: null
                        });
                    }
                }
            }
        } catch (e) {
            console.log('atrapando errores', e);
        }
    };

    handleClearFields = () => {
        this.setState({
            boxLength: '',
            boxWidth: '',
            boxHeight: '',
            boxVolume: '',
            boxWeight: '',
            boxVolumeWeight: '',
            boxItemsQuantity: '',
            boxContainer: '',
            tagNumber: '',
            boxSKU: ''
        });
    };

    checkProductWeight = async () => {
        this.setState({
            spinner: true,
            successAlert: false,
            errorAlert: false
        });
        try {
            const result = await requestParcelCubeInfo(this.state.boxSKU);
            const {status, response} = await result[0];
            console.log('respuesta', JSON.parse(response));
            console.log('status', status);
            const stringToArray = JSON.parse(response);
            switch (status) {
                case 200:
                    this.setState({
                        spinner: false,
                        boxLength: stringToArray[0],
                        boxWidth: stringToArray[1],
                        boxHeight: stringToArray[2],
                        boxVolume: stringToArray[3],
                        boxWeight: stringToArray[4],
                        boxVolumeWeight: stringToArray[5],
                        boxSKU: this.state.boxSKU,
                        //boxItemsQuantity: stringToArray[7],
                        //boxContainer: stringToArray[8],
                        errorAlert: false,
                        alertMsg: ''
                    });
                    break;
                case 500:
                    if (stringToArray.message === 'I/O error on GET request for "http://172.16.16.187:8080": Connection timed out: connect; nested exception is java.net.ConnectException: Connection timed out: connect') {
                        this.setState({
                            spinner: false,
                            errorAlert: true,
                            alertMsg: 'Ocurrió un error de conexión. Por favor intente nuevamente o verifique que el software de ParcelCube esté activado.'
                        });
                    }
                    break;
            }
        } catch (e) {
            console.log('atrapando el error', e);
        }
    };

    handleBoxLength = e => this.setState({boxLength: e.target.value});

    handleBoxWidth = e => this.setState({boxWidth: e.target.value});

    handleBoxHeight = e => this.setState({boxHeight: e.target.value});

    handleBoxVolume = e => this.setState({boxVolume: e.target.value});

    handleBoxWeight = e => this.setState({boxWeight: e.target.value});

    handleBoxVolumeWeight = e => this.setState({boxVolumeWeight: e.target.value});

    handleProductType = e => {
        if (parseInt(e.target.value) === 0) {
            this.setState({boxSKU: '', tipoSKU: ''});
            this.setState({toggleCheckbox: true});
        } else {
            this.setState({tagNumber: '', tipoSKU: ''});
            this.setState({toggleCheckbox: false});
        }
        this.setState({productType: parseInt(e.target.value)});
    };

    handleTagNumber = async e => {
        this.setState({tagNumber: e.target.value.toUpperCase()});
        if (e.target.value.length >= 8) {
            this.checkProductWeight();
        }
    };

    //handleBoxSKU = e => this.setState({boxSKU: e.target.value.substring(0, e.target.value.length - 1)});
    handleBoxSKU = async e => {
        this.setState({boxSKU: e.target.value});
        if (e.target.value.length >= 6) {
            this.checkProductWeight();
        }
    };

    handleBoxSkuKeypress = e => this.setState({boxSKU: e.target.value});

    handleBoxItemsQuantity = e => this.setState({boxItemsQuantity: e.target.value});

    handleBoxContainer = e => this.setState({boxContainer: e.target.value});

    handleRealBoxItemsQuantity = e => this.setState({realBoxItemsQuantity: e.target.value});


    /**
     * RECORDATORIO:
     * Quitar el último dígito del código de barra en producción
     * @param e
     * @returns {void|*|boolean}
     */
        //handleBarCode = e => this.setState({barCode: e.target.value.substring(0, e.target.value.length - 1)});
    handleBarCode = e => {
        this.setState({barCode: e.target.value});
        let alertMsg = '', bool = false;
        this.state.arrayHomework.map((item, index) => {
            console.log('true codigo barra', item);
            if (parseInt(item.codigoBarra) === parseInt(e.target.value)) {
                this.state.checkboxInputs[index] += 1;
                if (this.state.checkboxInputs[index] > item.cantidad) {
                    this.nonValid[index] = true;
                }
            }
        });
        this.setState({
            checkboxInputs: this.state.checkboxInputs,
            errorAlert: bool,
            alertMsg: alertMsg,
        });
        setTimeout(() => {
            this.setState({barCode: ''});
        }, 100);
    };

    handleCheckbox = e => this.setState({toggleCheckbox: e.target.checked});

    refreshQuantityValidation = index => {
        this.nonValid[index] = false;
        this.state.checkboxInputs[index] = 0;
        this.setState({
            checkboxInputs: this.state.checkboxInputs
        });
    };

    toggleInputType = () => {
        this.setState({
            toggleInputTextType: !this.state.toggleInputTextType
        })
    };

    replaceNewRecord = async tipoSKU => {
        const {
            boxSKU,
            boxLength,
            boxWidth,
            boxHeight,
            boxVolume,
            boxWeight,
            boxVolumeWeight,
            boxContainer,
            tagNumber,
            realBoxItemsQuantity,
            arrayHomework,
            modelCode
        } = this.state;
        const $button = $(".btn-save-new");
        $button.button("loading");
        console.log('tipoSKU', tipoSKU);
        if (tipoSKU === '') {
            const volumeArray = [tagNumber, boxLength, boxWidth, boxVolume, boxWeight, boxVolumeWeight, boxContainer, boxHeight, realBoxItemsQuantity, boxSKU, modelCode];
            volumeArray[9] = volumeArray[9].substring(0, volumeArray[9].length - 1);
            const result = boxSKU.length > 6 ? await saveNewRecordLongSku(volumeArray) : await saveNewRecord(volumeArray, true);
            const {status, response} = result;
            if (status === 200) {
                if (response) {
                    $button.button("reset");
                    $("#modalConfirmacion").modal("hide");
                    this.setState({
                        boxLength: '',
                        boxWidth: '',
                        boxHeight: '',
                        boxVolume: '',
                        boxWeight: '',
                        boxVolumeWeight: '',
                        tagNumber: '',
                        arrayHomework: [],
                        warningAlert: false,
                        successAlert: true,
                        errorAlert: false,
                        alertMsg: 'La volumetría fue grabada y actualizada.',
                        tipoSKU: ''
                    });
                }
            }
        }
        if (tipoSKU === 'S') {
            const volumeArray = [tagNumber, boxLength, boxWidth, boxVolume, boxWeight, boxVolumeWeight, boxContainer, boxHeight, realBoxItemsQuantity, boxSKU, modelCode];
            const result = await saveNewRecord(volumeArray);
            const {status, response} = result;
            if (status === 200) {
                if (response) {
                    $button.button("reset");
                    $("#modalConfirmacion").modal("hide");
                    this.setState({
                        boxLength: '',
                        boxWidth: '',
                        boxHeight: '',
                        boxVolume: '',
                        boxWeight: '',
                        boxVolumeWeight: '',
                        tagNumber: '',
                        arrayHomework: [],
                        warningAlert: false,
                        successAlert: true,
                        errorAlert: false,
                        alertMsg: 'La volumetría fue grabada y actualizada.',
                        tipoSKU: ''
                    });
                }
            }

        }
        if (tipoSKU === 'T') {
            const volumeArray = [tagNumber, boxLength, boxWidth, boxVolume, boxWeight, boxVolumeWeight, boxContainer, boxHeight, realBoxItemsQuantity, arrayHomework && arrayHomework.length > 0 ? arrayHomework[0].sku : null, modelCode];
            const result = await saveNewRecord(volumeArray);
            const {status, response} = result;
            if (status === 200) {
                if (response) {
                    if (arrayHomework[0].clase === 'CALZADO') {
                        //const result = await updateSkuWeightIntoPmf(arrayHomework[0].sku, boxWeight);
                        const result = await updateHomeworkWeightIntoPmf(arrayHomework[0].sku, boxWeight);
                        console.log('resultado de pmf', result);
                        const {status} = result;
                        if (status === 200) {
                            $button.button("reset");
                            $("#modalConfirmacion").modal("hide");
                            this.setState({
                                boxLength: '',
                                boxWidth: '',
                                boxHeight: '',
                                boxVolume: '',
                                boxWeight: '',
                                boxVolumeWeight: '',
                                tagNumber: '',
                                arrayHomework: [],
                                warningAlert: false,
                                successAlert: true,
                                errorAlert: false,
                                alertMsg: 'La volumetría fue grabada y actualizada.',
                                tipoSKU: ''
                            });
                        }
                    } else {
                        $button.button("reset");
                        $("#modalConfirmacion").modal("hide");
                        this.setState({
                            boxLength: '',
                            boxWidth: '',
                            boxHeight: '',
                            boxVolume: '',
                            boxWeight: '',
                            boxVolumeWeight: '',
                            tagNumber: '',
                            arrayHomework: [],
                            warningAlert: false,
                            successAlert: true,
                            errorAlert: false,
                            alertMsg: 'La volumetría fue grabada.',
                        });
                    }
                }
            }
        }
    };

    render() {
        const {
            successAlert,
            errorAlert,
            warningAlert,
            alertMsg,
            spinner,
            spinnerValidate,
            boxLength,
            boxWidth,
            boxHeight,
            boxVolume,
            boxWeight,
            boxVolumeWeight,
            boxSKU,
            boxItemsQuantity,
            boxContainer,
            tagNumber,
            productType,
            productDetail,
            realBoxItemsQuantity,
            arrayHomework,
            spinnerSearch,
            modelCode
        } = this.state;
        const volumeArray = [tagNumber, boxLength, boxWidth, boxVolume, boxWeight, boxVolumeWeight, boxContainer, boxHeight, realBoxItemsQuantity, arrayHomework && arrayHomework.length > 0 ? arrayHomework[0].sku : null, modelCode];
        console.log('model code', modelCode);
        return (
            <div>
                <div id="wrapper">
                    <div className="topbar">
                        <div className="navbar navbar-default" role="navigation">
                            <div className="container">
                                <div className="navbar-collapse2">
                                    <ul className="nav navbar-nav hidden-xs">
                                        <li>
                                            <a href="#" style={{
                                                paddingTop: 10
                                            }}>
                                                <img src="/assets/img/logo_forus.png" width={100}
                                                     alt="Logo"/>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul className="nav navbar-nav navbar-right top-navbar">
                                        <li className="dropdown iconify hide-phone">
                                            <a href="#" className="dropdown-toggle" data-toggle="dropdown"><i
                                                className="fa fa-archive"/><span
                                                className="label label-danger absolute">{this.state.duplicateSku !== null && this.state.duplicateSku}</span></a>
                                            <ul className="dropdown-menu dropdown-message">
                                                <li className="dropdown-header notif-header"><i
                                                    className="icon-bell-2"/> Notificaciones<a className="pull-right"
                                                                                               href="#"><i
                                                    className="fa fa-cog"/></a></li>
                                                {
                                                    this.state.duplicateSku !== null &&
                                                    <li className="unread">
                                                        <a href="#"
                                                           onClick={() => window.open('http://localhost:8086/admin', '_blank')}>
                                                            <p>
                                                                <strong>Hay {this.state.duplicateSku === '+1' ? `más de un producto repetido.` : `${this.state.duplicateSku} producto repetido.`}
                                                                </strong> Haga clic en "Ver más" para
                                                                verificar.
                                                            </p>
                                                        </a>
                                                    </li>
                                                }
                                                <li className="dropdown-footer">
                                                    <div className="btn-group btn-group-justified">
                                                        <div className="btn-group">
                                                            <button
                                                                onClick={() => window.open('http://localhost:8086/admin', '_blank')}
                                                                className="btn btn-sm btn-success">Ver más
                                                            </button>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                        {/*   <li className="dropdown iconify hide-phone">
                                            <a href="#" onClick={toggle_fullscreen()}>
                                                <i className="icon-resize-full-2"/>
                                            </a>
                                        </li>*/}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container" style={{paddingTop: 80}}>


                    {/*MODAL CONFIRMACION*/}
                    <div className="modal fade" id="modalConfirmacion" tabIndex="-1" role="dialog"
                         aria-labelledby="myModalLabel">
                        <div className="modal-dialog modal-lg" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h3 className="modal-title text-center" id="myModalLabel"><strong>Volumetría
                                        repetida. Verifique antes de
                                        guardar <i
                                            className={'fa fa-info-circle'}/></strong></h3>
                                </div>
                                <div className="modal-body">
                                    <div id="verificationTable"/>
                                    <hr/>
                                    <div id="currentRecordTable"/>
                                    {
                                        this.state.spinnerModal &&
                                        <SpinnerModal/>
                                    }
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-warning" data-dismiss="modal">Mantener
                                        previo
                                    </button>
                                    <button type="button" data-loading-text={'Guardando...'}
                                            className="btn btn-success btn-save-new"
                                            onClick={() => this.replaceNewRecord(this.state.tipoSKU)}>Reemplazar por
                                        nuevo
                                    </button>
                                    <button type="button" className="btn btn-primary" data-dismiss="modal">Cancelar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="row">
                        <div className="col-md-4">
                            <div className="login-wrap animated flipInX">
                                <div style={{marginBottom: 50}}>
                                    <div className="pull-left">
                                        <div className="btn-group">
                                            <button onClick={this.checkProductWeight}
                                                    className={'btn btn-primary'}
                                                    disabled={spinner}>
                                                <i className="fa fa-download"
                                                   style={{margin: 0, fontSize: 18}}/>
                                            </button>
                                            <button onClick={this.handleClearFields}
                                                    className={'btn btn-default'}
                                                    disabled={spinner}>
                                                <i className="fa fa-eraser"
                                                   style={{margin: 0, fontSize: 18}}/>
                                            </button>
                                        </div>
                                    </div>
                                    <div className="pull-right">
                                        {
                                            productType === 1 ?
                                                <button onClick={this.validateBySKU}
                                                        className={'btn btn-success'}
                                                        disabled={spinnerValidate}>
                                                    {!spinnerValidate ? <i className={'fa fa-save'}
                                                                           style={{margin: 0, fontSize: 18}}/> :
                                                        <Spinner/>}
                                                </button> :
                                                <button onClick={this.validateByTagNumber}
                                                        className={'btn btn-success'}
                                                        disabled={spinnerValidate}>
                                                    {!spinnerValidate ? <i className={'fa fa-save'}
                                                                           style={{margin: 0, fontSize: 18}}/> :
                                                        <Spinner/>}
                                                </button>
                                        }
                                    </div>
                                </div>
                                <div className="login-block"
                                     style={{clear: 'both'}}>
                                    <div className="text-center">
                                        <h3 style={{
                                            color: 'white',
                                            fontWeight: 'bold',
                                            marginBottom: '30px',
                                            fontSize: '1.5em'
                                        }}>Volumetría de productos {spinner && <Spinner/>}</h3>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            {
                                                productType === 0 ?
                                                    <div className="form-group">
                                                        <label htmlFor="" className="control-label">Nº TAG</label>
                                                        <input type="text"
                                                               value={tagNumber}
                                                               className="form-control"
                                                               onChange={this.handleTagNumber}/>
                                                    </div> :
                                                    <div className="form-group">
                                                        <label htmlFor="" className="control-label">SKU:</label>
                                                        <input type="text"
                                                               value={boxSKU}
                                                               id={'scannerInput'}
                                                               className="form-control"
                                                               onChange={this.handleBoxSKU}
                                                        />
                                                    </div>
                                            }
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label className={'control-label'}>Longitud:</label>
                                                <input type="text"
                                                       value={boxLength}
                                                       className="form-control"
                                                       onChange={this.handleBoxLength}/>
                                            </div>
                                            <div className="form-group">
                                                <label className={'control-label'}>Ancho:</label>
                                                <input type="text"
                                                       value={boxWidth}
                                                       className="form-control"
                                                       onChange={this.handleBoxWidth}/>
                                            </div>
                                            <div className="form-group">
                                                <label className={'control-label'}>Alto:</label>
                                                <input type="text"
                                                       value={boxHeight}
                                                       className="form-control"
                                                       onChange={this.handleBoxHeight}/>
                                            </div>
                                            <div className="form-group">
                                                <label className="control-label">Volumen:</label>
                                                <input type="text"
                                                       value={boxVolume}
                                                       className="form-control"
                                                       onChange={this.handleBoxVolume}/>
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label htmlFor="" className="control-label">Peso:</label>
                                                <input type="text"
                                                       value={boxWeight}
                                                       className="form-control"
                                                       onChange={this.handleBoxWeight}/>
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="" className="control-label">Vol. Peso</label>
                                                <input type="text"
                                                       value={boxVolumeWeight}
                                                       className="form-control"
                                                       onChange={this.handleBoxVolumeWeight}/>
                                            </div>
                                            {
                                                this.state.productType === 0 &&
                                                <div className="form-group">
                                                    <label htmlFor="" className="control-label">Unidades:</label>
                                                    <input type="number"
                                                           value={realBoxItemsQuantity}
                                                           className="form-control"
                                                           disabled={true}
                                                           onChange={this.handleRealBoxItemsQuantity}/>
                                                </div>
                                            }
                                            <div className="form-group">
                                                <label htmlFor="" className="control-label">Contenedor:</label>
                                                <input type="text"
                                                       disabled={true}
                                                       value={boxContainer}
                                                       className="form-control"
                                                       onChange={this.handleBoxContainer}/>
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label htmlFor="" className="control-label">Tipo:</label>
                                                <select name="" id="" className={'form-control'}
                                                        onChange={this.handleProductType}>
                                                    <option value={0}>Caja Master</option>
                                                    <option value={1}>Unidad</option>
                                                </select>
                                            </div>
                                            {/*         {
                                                productType === 0 ?
                                                    <div className="form-group">
                                                        <label htmlFor="" className="control-label">Nº TAG</label>
                                                        <input type="text"
                                                               value={tagNumber}
                                                               className="form-control"
                                                               onChange={this.handleTagNumber}/>
                                                    </div> :
                                                    <div className="form-group">
                                                        <label htmlFor="" className="control-label">SKU:</label>
                                                        <input type="text"
                                                               value={boxSKU}
                                                               id={'scannerInput'}
                                                               className="form-control"
                                                               onChange={this.handleBoxSKU}
                                                        />
                                                    </div>
                                            }*/}
                                            {
                                                this.state.productType === 0 &&
                                                <div className="form-group">
                                                    <label className="control-label">Verificar
                                                        cantidades:</label>
                                                    <div className="checkbox">
                                                        <label>
                                                            <input type="checkbox"
                                                                   checked={this.state.toggleCheckbox}
                                                                   onChange={this.handleCheckbox}/>
                                                        </label>
                                                    </div>
                                                </div>
                                            }
                                        </div>
                                    </div>

                                    {/*                                            <div className={'form-horizontal'}>
                                                <div className="form-group">
                                                    <label className={'col-sm-2 control-label'}>Longitud:</label>
                                                    <div className="col-sm-10">
                                                        <input type="text"
                                                               value={boxLength}
                                                               className="form-control"
                                                               onChange={this.handleBoxLength}/>
                                                    </div>
                                                </div>
                                                <div className="form-group">
                                                    <label className="col-sm-2 control-label">Ancho:</label>
                                                    <div className="col-sm-10">
                                                        <input type="text"
                                                               value={boxWidth}
                                                               className="form-control"
                                                               onChange={this.handleBoxWidth}/>
                                                    </div>
                                                </div>
                                                <div className="form-group">
                                                    <label className="col-sm-2 control-label">Alto:</label>
                                                    <div className="col-sm-10">
                                                        <input type="text"
                                                               value={boxHeight}
                                                               className="form-control"
                                                               onChange={this.handleBoxHeight}/>
                                                    </div>
                                                </div>
                                                <div className="form-group">
                                                    <label className="col-sm-2 control-label">Volumen:</label>
                                                    <div className="col-sm-10">
                                                        <input type="text"
                                                               value={boxVolume}
                                                               className="form-control"
                                                               onChange={this.handleBoxVolume}/>
                                                    </div>
                                                </div>
                                                <div className="form-group">
                                                    <label
                                                        className="col-sm-1 col-sm-push-1 control-label">Peso:</label>
                                                    <div className="col-sm-3 col-sm-push-1">
                                                        <input type="text"
                                                               value={boxWeight}
                                                               className="form-control"
                                                               onChange={this.handleBoxWeight}/>
                                                    </div>
                                                    <label className="col-sm-1 col-sm-push-1 control-label">=</label>
                                                    <div className="col-sm-3 col-sm-push-1">
                                                        <input type="text"
                                                               value={boxVolumeWeight}
                                                               className="form-control"
                                                               onChange={this.handleBoxVolumeWeight}/>
                                                    </div>
                                                    <label className="col-sm-2 col-sm-push-1">Vol. Peso</label>
                                                </div>
                                            </div>*/}
                                </div>
                                {/*                                        <div className="col-md-8" style={{paddingTop: 20}}>
                                            <div className="form-horizontal">
                                                <div className="form-group">
                                                    <label className="col-sm-2 control-label">Tipo producto:</label>
                                                    <div className="col-sm-10">
                                                        <select name="" id="" className={'form-control'}
                                                                onChange={this.handleProductType}>
                                                            <option value={0}>Caja Master</option>
                                                            <option value={1}>Unidad</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                {
                                                    productType === 0 ?
                                                        <div className="form-group">
                                                            <label className="col-sm-2 control-label">Nº TAG:</label>
                                                            <div className="col-sm-10">
                                                                <input type="text"
                                                                       value={tagNumber}
                                                                       className="form-control"
                                                                       onChange={this.handleTagNumber}/>
                                                            </div>
                                                        </div> :
                                                        <div className="form-group">
                                                            <label className="col-sm-2 control-label">SKU:</label>
                                                            <div className="col-sm-10">
                                                                <input type="text"
                                                                       value={boxSKU}
                                                                       id={'scannerInput'}
                                                                       className="form-control"
                                                                       onChange={this.handleBoxSKU}
                                                                />
                                                            </div>
                                                        </div>
                                                }
                                            </div>
                                            {
                                                this.state.productType === 0 &&
                                                <div className="form-horizontal">
                                                    <div className="form-group">
                                                        <label className="col-sm-2 control-label">Unidades caja:</label>
                                                        <div className="col-sm-10">
                                                            <input type="number"
                                                                   value={realBoxItemsQuantity}
                                                                   className="form-control"
                                                                   disabled={true}
                                                                   onChange={this.handleRealBoxItemsQuantity}/>
                                                        </div>
                                                    </div>
                                                </div>
                                            }
                                            <div className="form-horizontal">
                                                <div className="form-group">
                                                    <label className="col-sm-2 control-label">Contenedor:</label>
                                                    <div className="col-sm-10">
                                                        <input type="text"
                                                               disabled={true}
                                                               value={boxContainer}
                                                               className="form-control"
                                                               onChange={this.handleBoxContainer}/>
                                                    </div>
                                                </div>
                                            </div>
                                            {
                                                this.state.productType === 0 &&
                                                <div className="form-horizontal">
                                                    <div className="form-group">
                                                        <label className="col-sm-2 control-label">Verificar
                                                            cantidades:</label>
                                                        <div className="col-sm-10">
                                                            <div className="checkbox">
                                                                <label>
                                                                    <input type="checkbox"
                                                                           onChange={this.handleCheckbox}/>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            }
                                        </div>*/}
                                {/*  <button onClick={this.checkProductWeight}
                                        className={'btn btn-block btn-primary'}
                                        disabled={spinner}>
                                    {!spinner ? 'OBTENER PESO' : <Spinner/>}
                                </button>*/}
                                {/*      {
                                    productType === 1 ?
                                        <button onClick={this.validateBySKU}
                                                className={'btn btn-block btn-success'}
                                                disabled={spinnerValidate}>
                                            {!spinnerValidate ? 'GRABAR' : <Spinner/>}
                                        </button> :
                                        <button onClick={this.validateByTagNumber}
                                                className={'btn btn-block btn-success'}
                                                disabled={spinnerValidate}>
                                            {!spinnerValidate ? 'GRABAR' : <Spinner/>}
                                        </button>
                                }*/}

                                {/*{
                            null === productDetail &&
                            <div className="alert alert-danger text-center" style={{marginTop: 20}}>
                                <strong>Error!</strong> No se encontraron registros
                            </div>
                        }*/}
                                {
                                    successAlert !== false && alertMsg !== '' &&
                                    <div className="alert alert-success text-center" style={{marginTop: 20}}>
                                        <strong>¡Éxito!</strong> {alertMsg}
                                    </div>
                                }
                                {
                                    errorAlert !== false && alertMsg !== '' &&
                                    <div className="alert alert-danger text-center" style={{marginTop: 20}}>
                                        <strong>¡Error!</strong> {alertMsg}
                                    </div>
                                }
                                {
                                    warningAlert !== false && alertMsg !== '' &&
                                    <div className="alert alert-warning text-center" style={{marginTop: 20}}>
                                        <strong>¡Advertencia!</strong> {alertMsg}
                                    </div>
                                }
                            </div>
                        </div>
                        <div className="col-md-8" style={{paddingTop: 20}}>
                            {
                                arrayHomework.length > 0 &&
                                <div className="widget">
                                    <div className="widget-header">
                                        <h2><strong>Detalles</strong> de la tarea</h2>
                                    </div>
                                    <div className="widget-content">
                                        <div className="data-table-toolbar">
                                            <div className="row">
                                                {
                                                    this.state.toggleCheckbox &&
                                                    <div>
                                                        <div className="col-md-4">
                                                            <div className="form-inline">
                                                                <div className="form-group">
                                                                    <input type="number" className="form-control"
                                                                           id="filterByOrderNumber"
                                                                           placeholder="COD. BARRA"
                                                                           value={this.state.barCode}
                                                                           onChange={this.handleBarCode}
                                                                    />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-8">
                                                            <div className="toolbar-btn-action">
                                                                <button className={'btn btn-success'}
                                                                        disabled={this.state.spinnerSave}
                                                                        onClick={
                                                                            async () => {
                                                                                this.setState({spinnerSave: true});
                                                                                const responseCheck = await checkRecordBySKU(parseInt(arrayHomework[0].sku));
                                                                                const {status, response} = responseCheck[0];
                                                                                if (status === 200) {
                                                                                    if (response > 0) {
                                                                                        this.setState({spinnerSave: false});
                                                                                        let modalConfirmacion = $("#modalConfirmacion");
                                                                                        modalConfirmacion.modal("show");

                                                                                        modalConfirmacion.on("shown.bs.modal", async () => {
                                                                                            this.setState({spinnerModal: true});
                                                                                            const responsePreviousRecord = await requestPreviousRecordByTagNumber(this.state.tagNumber);
                                                                                            console.log('responsePreviousRecord', responsePreviousRecord);
                                                                                            const {status, response} = responsePreviousRecord;
                                                                                            if (status === 200) {
                                                                                                this.setState({spinnerModal: false});
                                                                                                const data = response[0];
                                                                                                let previousRecord = '<div className="widget">\n' +
                                                                                                    '                                        <div className="widget-header">\n' +
                                                                                                    '                                            <div className="alert alert-warning"><h4><strong>Volumetría previa</strong></h4></div>\n' +
                                                                                                    '                                        </div>\n' +
                                                                                                    '                                        <div className="widget-content">\n' +
                                                                                                    '                                        <br/>\n' +
                                                                                                    '                                       <div className="table-responsive">\n' +
                                                                                                    '                                            <table className="table table-condensed table-bordered tablePreviousRecord" width="100%">\n' +
                                                                                                    '                                                <thead>\n' +
                                                                                                    '                                                <tr>\n' +
                                                                                                    '                                                    <th>SKU</th>\n' +
                                                                                                    '                                                    <th>LARGO</th>\n' +
                                                                                                    '                                                    <th>ANCHO</th>\n' +
                                                                                                    '                                                    <th>VOLUMEN</th>\n' +
                                                                                                    '                                                    <th>PESO</th>\n' +
                                                                                                    '                                                    <th>VOL. PESO</th>\n' +
                                                                                                    '                                                    <th>CONTENEDOR</th>\n' +
                                                                                                    '                                                    <th>F. CREACIÓN</th>\n' +
                                                                                                    '                                                    <th>UNID. CAJA</th>\n' +
                                                                                                    '                                                    <th>ALTURA</th>\n' +
                                                                                                    '                                                    <th>Nº TAG</th>\n' +
                                                                                                    '                                                    <th>COD. MODELO</th>\n' +
                                                                                                    '                                                </tr>\n' +
                                                                                                    '                                                </thead>\n' +
                                                                                                    '                                                <tbody>\n' +
                                                                                                    '                                                <tr>\n' +
                                                                                                    '                                                    <td>' + data[0] + '</td>\n' +
                                                                                                    '                                                    <td>' + data[1] + '</td>\n' +
                                                                                                    '                                                    <td>' + data[2] + '</td>\n' +
                                                                                                    '                                                    <td>' + data[3] + '</td>\n' +
                                                                                                    '                                                    <td>' + data[4] + '</td>\n' +
                                                                                                    '                                                    <td>' + data[5] + '</td>\n' +
                                                                                                    '                                                    <td>' + data[6] + '</td>\n' +
                                                                                                    '                                                    <td>' + moment(data[11]).format('LLL') + '</td>\n' +
                                                                                                    '                                                    <td>' + data[12] + '</td>\n' +
                                                                                                    '                                                    <td>' + data[13] + '</td>\n' +
                                                                                                    '                                                    <td>' + data[14] + '</td>\n' +
                                                                                                    '                                                    <td>' + data[16] + '</td>\n' +
                                                                                                    '                                                </tr>\n' +
                                                                                                    '                                                </tbody>\n' +
                                                                                                    '                                            </table>\n' +
                                                                                                    '                                            </div>\n' +
                                                                                                    '                                        </div>\n' +
                                                                                                    '                                    </div>';


                                                                                                let currentRecord = ' <div className="widget">\n' +
                                                                                                    '                                        <div className="widget-header">\n' +
                                                                                                    '                                            <div className="alert alert-info"><h4><strong>Volumetría actual</strong></h4></div>\n' +
                                                                                                    '                                        </div>\n' +
                                                                                                    '                                        <div className="widget-content">\n' +
                                                                                                    '                                        <br/>\n' +
                                                                                                    '                                            <div className="table-responsive">\n' +
                                                                                                    '                                                <table\n' +
                                                                                                    '                                                    className="table table-condensed table-bordered tableCurrentRecord" width="100%">\n' +
                                                                                                    '                                                    <thead>\n' +
                                                                                                    '                                                    <tr>\n' +
                                                                                                    '                                                        <th>SKU</th>\n' +
                                                                                                    '                                                        <th>LARGO</th>\n' +
                                                                                                    '                                                        <th>ANCHO</th>\n' +
                                                                                                    '                                                        <th>VOLUMEN</th>\n' +
                                                                                                    '                                                        <th>PESO</th>\n' +
                                                                                                    '                                                        <th>VOL. PESO</th>\n' +
                                                                                                    '                                                        <th>CONTENEDOR</th>\n' +
                                                                                                    '                                                        <th>F. CREACIÓN</th>\n' +
                                                                                                    '                                                        <th>UNID. CAJA</th>\n' +
                                                                                                    '                                                        <th>ALTURA</th>\n' +
                                                                                                    '                                                        <th>Nº TAG</th>\n' +
                                                                                                    '                                                        <th>COD. MODELO</th>\n' +
                                                                                                    '                                                    </tr>\n' +
                                                                                                    '                                                    </thead>\n' +
                                                                                                    '                                                    <tbody>\n' +
                                                                                                    '                                                    <tr>\n' +
                                                                                                    '                                                        <td>' + arrayHomework[0].sku + '</td>\n' +
                                                                                                    '                                                        <td>' + this.state.boxLength + '</td>\n' +
                                                                                                    '                                                        <td>' + this.state.boxWidth + '</td>\n' +
                                                                                                    '                                                        <td>' + this.state.boxVolume + '</td>\n' +
                                                                                                    '                                                        <td>' + this.state.boxWeight + '</td>\n' +
                                                                                                    '                                                        <td>' + this.state.boxVolumeWeight + '</td>\n' +
                                                                                                    '                                                        <td>' + this.state.boxContainer + '</td>\n' +
                                                                                                    '                                                        <td>Actual</td>\n' +
                                                                                                    '                                                        <td>' + this.state.realBoxItemsQuantity + '</td>\n' +
                                                                                                    '                                                        <td>' + this.state.boxHeight + '</td>\n' +
                                                                                                    '                                                        <td>' + this.state.tagNumber + '</td>\n' +
                                                                                                    '                                                        <td>' + this.state.modelCode + '</td>\n' +
                                                                                                    '                                                    </tr>\n' +
                                                                                                    '                                                    </tbody>\n' +
                                                                                                    '                                                </table>\n' +
                                                                                                    '                                            </div>\n' +
                                                                                                    '                                        </div>\n' +
                                                                                                    '                                    </div>';

                                                                                                $("#verificationTable").html(previousRecord);
                                                                                                $("#currentRecordTable").html(currentRecord);
                                                                                            }
                                                                                        });


                                                                                        // const result = await saveVolumeByTagNumber(volumeArray, true);
                                                                                        // const {status, response} = result[0];
                                                                                        // console.log('result guardar volumetria', result);
                                                                                        // if (status === 200) {
                                                                                        //     if (response) {
                                                                                        //         this.setState({
                                                                                        //             warningAlert: true,
                                                                                        //             alertMsg: 'Este SKU ya se encuentra registrado, revisar duplicados.',
                                                                                        //             spinnerSave: false,
                                                                                        //             productDetail: null,
                                                                                        //             boxLength: '',
                                                                                        //             boxWidth: '',
                                                                                        //             boxHeight: '',
                                                                                        //             boxVolume: '',
                                                                                        //             boxWeight: '',
                                                                                        //             boxVolumeWeight: '',
                                                                                        //             tagNumber: '',
                                                                                        //         });
                                                                                        //     }
                                                                                        // }
                                                                                    } else {
                                                                                        const result = await saveVolumeByTagNumber(volumeArray);
                                                                                        const {status, response} = result[0];
                                                                                        if (status === 200) {
                                                                                            if (response) {
                                                                                                this.setState({
                                                                                                    warningAlert: false,
                                                                                                    successAlert: true,
                                                                                                    alertMsg: 'La volumetría fue grabada.',
                                                                                                    spinnerSave: false,
                                                                                                    productDetail: null,
                                                                                                    boxLength: '',
                                                                                                    boxWidth: '',
                                                                                                    boxHeight: '',
                                                                                                    boxVolume: '',
                                                                                                    boxWeight: '',
                                                                                                    boxVolumeWeight: '',
                                                                                                    tagNumber: '',
                                                                                                });

                                                                                                console.log('contenido array homework', arrayHomework);

                                                                                                if (arrayHomework[0].clase === 'CALZADO') {
                                                                                                    //const result = await updateSkuWeightIntoPmf(arrayHomework[0].sku, boxWeight);
                                                                                                    const result = await updateHomeworkWeightIntoPmf(arrayHomework[0].sku, boxWeight);
                                                                                                    console.log('resultado de pmf', result);
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                >
                                                                    <i className="fa fa-save"/> {this.state.spinnerSave ? 'Verificando...' : 'Guardar volumetría'}
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                }
                                                {/* <div className={this.state.toggleCheckbox ? 'col-md-8' : 'col-md-8 col-md-push-4'}>
                                        <div className="toolbar-btn-action">
                                            <button className="btn btn-success"
                                                    onClick={this.checkQuantities}>{!this.state.toggleCheckbox ? 'Validar cantidades' : 'Cancelar'}</button>
                                        </div>
                                    </div>*/}
                                            </div>
                                        </div>
                                        <table className={'table table-bordered table-condensed'}>
                                            <thead>
                                            <tr>
                                                <th>CANTIDAD</th>
                                                <th>SKU</th>
                                                <th>COD. MODELO</th>
                                                <th>COD. COLOR</th>
                                                <th>MODELO</th>
                                                {
                                                    arrayHomework[0].tallaNumero !== null &&
                                                    <th>TALLA</th>
                                                }
                                                <th>COD. BARRA</th>
                                                {
                                                    this.state.toggleCheckbox &&
                                                    <th>VERIFICACIÓN</th>
                                                }
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                arrayHomework.map((item, index) => {
                                                    console.log('item sku array', item.tallaNumero);
                                                    return (
                                                        <tr key={index}>
                                                            <td className={'text-left'}>{item.cantidad}</td>
                                                            <td className={'text-left'}>{item.sku}</td>
                                                            <td className={'text-left'}>{item.codigoModelo}</td>
                                                            <td className={'text-left'}>{item.codigoColor}</td>
                                                            <td className={'text-left'}>{item.modelo}</td>
                                                            {
                                                                item.tallaNumero !== null &&
                                                                <td className={'text-left'}>{item.tallaNumero}</td>
                                                            }
                                                            <td className={'text-left'}>{item.codigoBarra}</td>
                                                            {
                                                                this.state.toggleCheckbox &&
                                                                <td>
                                                                    <div style={{display: 'inline-flex'}}>
                                                                        <input type="number"
                                                                               className={this.state.checkboxInputs[index] > 0 && this.state.checkboxInputs[index] <= item.cantidad ? 'form-control validField' : this.state.checkboxInputs[index] > item.cantidad ? 'form-control nonValidField' : 'form-control'}
                                                                               size={'4'}
                                                                               value={this.state.checkboxInputs[index]}
                                                                               onChange={e => this.handleChangeInputCheckbox(index, e.target.value)}
                                                                               disabled={true}
                                                                               style={{
                                                                                   width: 80,
                                                                                   margin: 'auto'
                                                                               }}/>
                                                                        {
                                                                            this.nonValid[index] &&
                                                                            <button
                                                                                onClick={() => this.refreshQuantityValidation(index)}
                                                                                className="btn btn-primary btn-xs"
                                                                                style={{marginLeft: 5}}>
                                                                                <i className="fa fa-refresh"/>
                                                                            </button>
                                                                        }
                                                                    </div>
                                                                    {
                                                                        this.nonValid[index] && <div>
                                                                <span
                                                                    className="nonValidLabel">Excede cantidad</span>
                                                                        </div>
                                                                    }
                                                                </td>
                                                            }
                                                        </tr>
                                                    )
                                                })
                                            }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            }


                            {
                                productDetail !== null &&
                                <div className="widget">
                                    <div className="widget-header">
                                        <h2><strong>Detalles</strong> del sólido</h2>
                                    </div>
                                    <div className="widget-content">
                                        <table className={'table table-bordered table-condensed'}>
                                            <thead>
                                            <tr>
                                                {
                                                    productDetail.cantidad !== null &&
                                                    <th>CANTIDAD</th>
                                                }
                                                <th>SKU</th>
                                                <th>TIPO</th>
                                                <th>COD. MODELO</th>
                                                <th>COD. COLOR</th>
                                                <th>MODELO</th>
                                                <th>COLOR</th>
                                                {
                                                    productDetail.talla !== null &&
                                                    <th>TEMPORADA</th>
                                                }
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                {
                                                    productDetail.cantidad !== null &&
                                                    <td className={'text-left'}>{productDetail.cantidad}</td>
                                                }
                                                <td className={'text-left'}>{productDetail.sku}</td>
                                                <td className={'text-left'}>{productDetail.tipoSKU === 'S' && 'SÓLIDO'}</td>
                                                <td className={'text-left'}>{productDetail.codigoModelo}</td>
                                                <td className={'text-left'}>{productDetail.codigoColor}</td>
                                                <td className={'text-left'}>{productDetail.modelo}</td>
                                                <td className={'text-left'}>{productDetail.color}</td>
                                                {
                                                    productDetail.talla !== null &&
                                                    <td className={'text-left'}>{productDetail.talla}</td>
                                                }
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>
                {/*                <div className="full-content-center">


                </div>*/}
            </div>
        );
    }
}

ReactDOM.render(<FormParcelCube/>, document.getElementById('formParcelCube'));