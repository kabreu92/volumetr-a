'use strict';

async function requestAuthentication(user, password) {
    const response = await fetch(`http://localhost:8086/requestAuthentication?user=${user}&password=${password}`);
    return Promise.all([{
        status: response.status,
        response: await response.json()
    }]);
}

class AuthorizationComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: '',
            password: ''
        };
    }

    handleUser = e => this.setState({user: e.target.value});

    handlePassword = e => this.setState({password: e.target.value});

    handleAuth = async () => {
        try {
            const result = await requestAuthentication(this.state.user, this.state.password);
            console.log('resultado', result);
            const {status, response} = result[0];
            if (status === 200) {
                if (response.length === 0) {
                    return;
                }
                if (response.length > 0) {
                    $("#authorizationModal").modal('hide');
                }
            }
        } catch (e) {
            console.log(e);
        }
    };

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <button className={'btn btn-success'}
                                data-toggle="modal"
                                data-target="#authorizationModal"
                                style={{
                                    float: 'right',
                                    margin: '0px 10%'
                                }}><i className="fa fa-lock"/> Autorización
                        </button>
                    </div>
                </div>

                <div className="modal fade" id="authorizationModal" tabIndex="-1" role="dialog"
                     aria-labelledby="myModalLabel">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                                <h4 className="modal-title" id="myModalLabel"><strong>Iniciar sesión</strong></h4>
                            </div>
                            <div className="modal-body">
                                <div className="form-group">
                                    <label className={'pull-left'}>Usuario:</label>
                                    <input type="text"
                                           onChange={this.handleUser}
                                           value={this.state.user}
                                           className="form-control"/>
                                </div>
                                <div className="form-group">
                                    <label className={'pull-left'}>Contraseña</label>
                                    <input type="password"
                                           onChange={this.handlePassword}
                                           value={this.state.password}
                                           className="form-control"/>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button"
                                        className="btn btn-default"
                                        data-dismiss="modal">Cancelar</button>
                                <button type="button"
                                        onClick={this.handleAuth}
                                        className="btn btn-primary">Autenticar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

ReactDOM.render(<AuthorizationComponent/>, document.getElementById('authorizationComponent'));