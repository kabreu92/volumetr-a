'use strict';

let container;

async function saveWeightIntoPmf(weight, sku) {
    const response = await fetch(`http://localhost:8086/saveWeightIntoPmf?weight=${weight}&sku=${sku}`);
    return Promise.all([{
        status: response.status,
        response: await response.json()
    }]);
}


async function requestAuthentication(user, password) {
    const response = await fetch(`http://localhost:8086/requestAuthentication?user=${user}&password=${password}`);
    return Promise.all([{
        status: response.status,
        response: await response.json()
    }]);
}

async function requestDuplicatedProducts() {
    const response = await fetch(`http://localhost:8086/requestDuplicatedProducts`);
    return Promise.all([{
        status: response.status,
        response: await response.json()
    }]);
}

async function requestDeclineNewVolume(sku) {
    const response = await fetch(`http://localhost:8086/requestDeclineNewVolume?sku=${sku}`);
    return Promise.all([{
        status: response.status,
        response: await response.json()
    }]);
}

async function requestSaveNewVolume(sku) {
    const response = await fetch(`http://localhost:8086/requestSaveNewVolume?sku=${sku}`);
    return Promise.all([{
        status: response.status,
        response: await response.json()
    }]);
}

async function updateSkuWeightIntoPmf(sku, weight) {
    const response = await fetch(`http://localhost:8086/updateSkuWeightIntoPmf?sku=${sku}&weight=${weight}`);
    return Promise.all([{
        status: response.status,
        response: await response.json()
    }]);
}

async function updateHomeworkWeightIntoPmf(sku, weight) {
    const response = await fetch(`http://localhost:8086/updateHomeworkWeightIntoPmf?sku=${sku}&weight=${weight}`);
    return Promise.resolve({
        status: response.status,
        response: await response.json()
    });
}

async function showOriginalRecord(data, index) {
    const showBtn = $(`#showBtn-${index}`);
    showBtn.button("loading");
    const response = await fetch(`http://localhost:8086/requestOriginalRecord?sku=${parseInt(data)}`);
    const {status} = response;
    const resp = await response.json();
    if (status === 200) {
        container.setState({
            showOriginalRecord: true
        });
        $("#tableOriginalRecord").DataTable({
            data: resp,
            language: {
                url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            ordering: true,
            dom: 'Bfrtip',
            paging: false,
            searching: false,
            destroy: true,
            columns: [
                {
                    title: 'SKU'
                },
                {
                    title: 'LARGO'
                },
                {
                    title: 'ANCHO'
                },
                {
                    title: 'VOLUMEN'
                },
                {
                    title: 'PESO'
                },
                {
                    title: 'VOL. PESO'
                },
                {
                    title: 'CONTENEDOR'
                },
                {
                    title: 'FECHA DE CREACIÓN',
                    render: function (data) {
                        return "<span>" + moment(data).format('DD-MM-YYYY HH:mm:ss') + "</span>";
                    }
                },
                {
                    title: 'UNIDADES CAJA'
                },
                {
                    title: 'ALTURA'
                },
                {
                    title: 'Nº TAG'
                }
            ]
        });
        showBtn.button("reset");
    }
}

class Admin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: '',
            password: '',
            showOriginalRecord: false,
            isAuthenticated: false
        };
        container = this;
        this.dataTable = null;
    }

    handleUser = e => this.setState({user: e.target.value.toUpperCase()});

    handlePassword = e => this.setState({password: e.target.value});

    handleAuth = async () => {
        const authBtn = $("#authBtn");
        try {
            authBtn.button("loading");
            const result = await requestAuthentication(this.state.user, this.state.password);
            const {status, response} = result[0];
            if (status === 200) {
                if (response.length === 0) {
                    alert('Credenciales incorrectas');
                    this.setState({
                        isAuthenticated: false
                    });
                    authBtn.button("reset");
                    return;
                }
                if (response.length > 0) {
                    $("#authModal").modal('hide');
                    this.setState({
                        isAuthenticated: true
                    });
                    authBtn.button("reset");
                }
            } else {
                alert('Credenciales incorrectas');
                this.setState({
                    isAuthenticated: false
                });
                authBtn.button("reset");
            }
        } catch (e) {
            console.log(e);
        }
    };

    async componentDidMount() {
        try {
            const result = await requestDuplicatedProducts();
            console.log('veamos', result);
            const {status, response} = result[0];
            if (status === 200) {
                this.dataTable = $('#tableDuplicatedSku').DataTable({
                    data: response,
                    language: {
                        url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                    },
                    ordering: true,
                    dom: 'Bfrtip',
                    "sScrollX": false,
                    paging: true,
                    createdRow: function (row, data, dataIndex) {
                        $(row).addClass('skuRow-' + data[0]);
                    },
                    columns: [
                        {
                            title: 'SKU',
                        },
                        {
                            title: 'TIPO PRODUCTO',
                            render: function(data, data2, data3) {
                                console.log('data3[12]', data3[0].toString().length);
                                if (data3[12] === null && data3[0].toString().length === 6) {
                                    return "<span>UNIDAD CAJA MASTER</span>";
                                } else {
                                    return "<span>" + data + "</span>";
                                }
                            }
                        },
                        {
                            title: 'CLASE'
                        },
                        {
                            title: 'LARGO'
                        },
                        {
                            title: 'ANCHO'
                        },
                        {
                            title: 'VOLUMEN'
                        },
                        {
                            title: 'PESO'
                        },
                        {
                            title: 'VOL. PESO'
                        },
                        {
                            title: 'CONTENEDOR'
                        },
                        {
                            title: 'FECHA DE CREACIÓN',
                            render: function (data) {
                                return "<span>" + moment(data).format('DD-MM-YYYY HH:mm a') + "</span>";
                            }
                        },
                        {
                            title: 'UNIDADES CAJA'
                        },
                        {
                            title: 'ALTURA'
                        },
                        {
                            title: 'Nº TAG'
                        },
                        {
                            title: 'ACCIONES',
                            fnCreatedCell: (td, cellData, rowData, row, col) => {
                                /*         console.log('rowData', rowData);
                                         console.log('row', row);
                                         console.log('col', col);
                                         console.log('cellData', cellData);*/
                                ReactDOM.render(
                                    <div className="btn-group btn-group-xs">
                                        <button type={'button'}
                                                data-loading-text="Cargando..."
                                                id={`showBtn-${row}`}
                                                onClick={() => showOriginalRecord(rowData[0], row)}
                                                className="btn btn-default btn-xs">
                                            <i className={'fa fa-eye'}/>
                                        </button>
                                        <button type={'button'}
                                                className="btn btn-success btn-xs"
                                                data-loading-text="Cargando..."
                                                id={'updateBtn'}
                                                onClick={() => this.saveNewVolume(rowData, row)}><i
                                            className={'fa fa-check'}/></button>
                                        <button type={'button'}
                                                className="btn btn-danger btn-xs"
                                                data-loading-text="Cargando..."
                                                id={'declineBtn'}
                                                onClick={() => this.declineNewVolume(rowData[0], row)}><i
                                            className={'fa fa-remove'}/></button>
                                    </div>
                                    , td);
                            }
                        }
                    ]
                });
            }
        } catch (e) {
            console.log('catched errors', e);
        }
    }

    saveNewVolume = async (rowData, tableRow) => {
        try {
            if (this.state.isAuthenticated) {
                const updateBtn = $("#updateBtn");
                updateBtn.button('loading');
                rowData[9] = moment(rowData[9]).format("DD-MM-YYYY HH:mm:ss");
                if (rowData[1] === 'UNIDAD') {
                    console.log('llego aca unidad');
                    const resultPmf = await updateSkuWeightIntoPmf(rowData[0], rowData[6]);
                    const {status, response} = resultPmf[0];
                    if (status) {
                        if (response) {
                            const result = await requestSaveNewVolume(rowData);
                            const {status, response} = result[0];
                            console.log('result save new', result);
                            if (status === 200) {
                                if (response) {
                                    updateBtn.button('reset');
                                    this.dataTable
                                        .rows('.skuRow-' + rowData[0])
                                        .remove()
                                        .draw();
                                }
                            }
                        }
                    }
                    return;
                }
                if (rowData[2] === 'CALZADO') {
                    console.log('llego aca calzado');
                    const resultPmf = await updateHomeworkWeightIntoPmf(rowData[0], rowData[6]);
                    const {status, response} = resultPmf;
                    if (status) {
                        if (response) {
                            const result = await requestSaveNewVolume(rowData);
                            const {status, response} = result[0];
                            console.log('result save new', result);
                            if (status === 200) {
                                if (response) {
                                    updateBtn.button('reset');
                                    this.dataTable
                                        .rows('.skuRow-' + rowData[0])
                                        .remove()
                                        .draw();
                                }
                            }
                        }
                    }
                    return;
                }
                console.log('llego aca normal');
                const result = await requestSaveNewVolume(rowData);
                const {status, response} = result[0];
                console.log('result save new', result);
                if (status === 200) {
                    if (response) {
                        updateBtn.button('reset');
                        this.dataTable
                            .rows('.skuRow-' + rowData[0])
                            .remove()
                            .draw();
                    }
                }

            } else {
                $("#authModal").modal('toggle');
            }
        } catch (e) {
            console.log(e);
        }
    };

    declineNewVolume = async (sku, tableRow) => {
        try {
            if (this.state.isAuthenticated) {
                const declineBtn = $("#declineBtn"),
                    table = $("#tableDuplicated");
                declineBtn.button('loading');
                const result = await requestDeclineNewVolume(sku);
                console.log('resultado declinar', result);
                const {status, response} = result[0];
                if (status === 200) {
                    if (response) {
                        declineBtn.button('reset');
                        this.dataTable
                            .rows('.skuRow-' + sku)
                            .remove()
                            .draw();
                    }
                }
            } else {
                $("#authModal").modal('toggle');
            }
        } catch (e) {
            console.log(e);
        }
    };

    renderActionColumn = (data, row, value) => {
        console.log('valores datatable duplicado', value);
        console.log('row', row);
        console.log('data', data);
        let isAuthenticated = this.state.isAuthenticated;
        return ReactDOM.render(
            <button>Hola mundo</button>
            , row);
        /*        return (
                    "<div class='btn-group btn-group-xs'>" +
                    "<button type='button' class='btn btn-xs btn-default' onclick='showOriginalRecord(" + value[0] + ")'>" +
                    "<i class='fa fa-eye'></i>" +
                    "</button>" +
                    "<button type='button' class='btn btn-xs btn-success' data-toggle='modal' data-target='#authModal' onclick='saveNewVolume(" + isAuthenticated + ")'>" +
                    "<i class='fa fa-check'></i>" +
                    "</button>" +
                    "<button type='button' class='btn btn-xs btn-danger' data-toggle='modal' data-target='#authModal' onclick='declineNewVolume(" + isAuthenticated + ")'>" +
                    "<i class='fa fa-remove'></i>" +
                    "</button>" +
                    "</div>"
                );*/
    };

    render() {
        return (
            <div style={{
                width: '100%',
                padding: '5px 0',
                margin: '6% auto',
                textAlign: 'center',
            }}>
                {
                    this.state.showOriginalRecord && <div className={'container'}>
                        <div className="row">
                            <div className="col-md-12">
                                <table id="tableOriginalRecord"
                                       className={'table table-striped table-bordered display nowrap'}
                                       style={{
                                           width: '100%'
                                       }}/>
                            </div>
                        </div>
                    </div>
                }
                <div className="widget">
                    <div className="widget-header">
                        <h2 className={'text-left'}>
                            <strong>SKU</strong> repetidos
                        </h2>
                        <div className="additional-btn">
                            <a href="#" className="widget-toggle">
                                <i className="icon-down-open-2"/>
                            </a>
                        </div>
                    </div>
                    <div className="widget-content padding">
                        <br/>
                        <div className="table-responsive text-justify nowrap" id={'tableDuplicated'}>
                            <form className='form-horizontal' role='form'>
                                <table id="tableDuplicatedSku"
                                       className={'table table-striped table-bordered display nowrap'}
                                       style={{
                                           width: '100%'
                                       }}/>
                            </form>
                        </div>
                    </div>
                </div>


                <div className="modal fade" id="authModal" tabIndex="-1" role="dialog"
                     aria-labelledby="myModalLabel">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                                <h4 className="modal-title" id="myModalLabel"><strong>Necesitas permisos de
                                    supervisor</strong></h4>
                            </div>
                            <div className="modal-body">
                                <div className="form-group">
                                    <label className={'pull-left'}>Usuario:</label>
                                    <input type="text"
                                           onChange={this.handleUser}
                                           value={this.state.user}
                                           className="form-control"/>
                                </div>
                                <div className="form-group">
                                    <label className={'pull-left'}>Contraseña</label>
                                    <input type="password"
                                           onChange={this.handlePassword}
                                           value={this.state.password}
                                           className="form-control"/>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button"
                                        className="btn btn-default"
                                        data-dismiss="modal">Cancelar
                                </button>
                                <button type="button"
                                        onClick={this.handleAuth}
                                        data-loading-text={'Cargando...'}
                                        id={'authBtn'}
                                        className="btn btn-primary">Autenticar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

ReactDOM.render(<Admin/>, document.getElementById('adminComponent'));